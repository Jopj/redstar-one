#include <Arduino.h>
#include "RedstarClasses.h"


Beeperer::Beeperer(uint8_t pin) {
  _pin = pin;
  _beepables = false;
  _noteNumber = 0;
  _noteCounter = 0;
  _currentIndex = 0;
  for (unsigned int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
    _queue[i] = -1; //Initialize all members as unused
  }

}
void Beeperer::Run() {
  if (Beepables()) {
    if (_queue[_currentIndex] >= 0) {
      //Serial.print("playing tune "); Serial.print(_queue[_currentIndex]); Serial.print(" note "); Serial.print(_noteNumber); Serial.print(" counts "); Serial.println(_noteCounter);
      if (_noteNumber < tunelist[_queue[_currentIndex]].noteCount) {

        if (tunelist[_queue[_currentIndex]].notelist[_noteNumber].freq == 0) { //Silent note if frequency is 0
          noTone(_pin);
        }
        else {                              //Else play the note
          tone(_pin, tunelist[_queue[_currentIndex]].notelist[_noteNumber].freq);
        }

        if (_noteCounter < tunelist[_queue[_currentIndex]].notelist[_noteNumber].tim) {
          _noteCounter++;
        }
        else {
          _noteCounter = 0;
          _noteNumber++;
        }
      }
      else {        //Reached end of current tune, mark current index as empty and zero counters
        _queue[_currentIndex] = -1;
        _noteCounter = 0;
        _noteNumber = 0;
        noTone(_pin);
      }
    }
    else {
      //Serial.println("Finding new tune");
      int next = _GetNext();
      if (next < 0) {
        _beepables = false; //Record whether or not there are things in queue
      }
      else {
        _currentIndex = next;
        //Serial.print("Current index now "); Serial.print(_currentIndex); Serial.print(" and has tone "); Serial.println(_queue[_currentIndex]);
        _beepables = true;
      }
    }
  }
}

void Beeperer::AddToQueue(unsigned int beepType) {
  if (beepType >= 0 && beepType < (sizeof(tunelist) / sizeof(tunelist[0]))) {  //If beep type is within bounds
    //
    //    for (int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
    //      Serial.print(_queue[i]);
    //    }
    //    Serial.println();

    for (unsigned int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
      //Serial.print(_queue[i]);
      if (_queue[i] < 0) {
        _queue[i] = beepType;                              //Insert into first free slot
        _beepables = true;  //There must now be things in the queue as we just put one in
        //Serial.print("Added tone "); Serial.print(beepType); Serial.print(" to index "); Serial.println(i);
        break;
      }
    }
  }
}

bool Beeperer::Beepables() {
  return _beepables;
}

int32_t Beeperer::_GetNext() {  //Returns index of a valid tune, or negative if there are none
  int32_t returnable = -1;
  for (unsigned int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
    if (_queue[i] >= 0) {
      returnable = i; //return index of first item that is valid (not negative);
      //      Serial.print("Found tune "); Serial.print(returnable); Serial.print(" at index "); Serial.println(i);
      //
      //      for (int j = 0 ; j < (sizeof(_queue) / sizeof(_queue[0])) ; j++) {
      //        Serial.print(_queue[j]);
      //      }
      //      Serial.println();
      break;
    }
  }
  //if (returnable < 0) Serial.println("Found no new tunes");
  return returnable;
}


EventList::EventList(int numEvents) {
  //Do nothing, list is hardcoded to 8 instances
}

int EventList::PushBack(int id, uint32_t timestamp) {

  for (int i = 0 ; i < GetSlotCount() ; i++) { //Check if this even is already in list, if yes, update that
    if (eventList[i].event == id) {
      eventList[i].timestamp = timestamp;
      return 1;  //Found and replaced an event
    }
  }

  for (int i = 0 ; i < GetSlotCount() ; i++) { //If not, check if thre are empty spaces in list. If yes, put it in there
    if (eventList[i].event == 0) {
      eventList[i].event = id;
      eventList[i].timestamp = timestamp;
      return 2;  //Added new event
    }
  }
  
  uint32_t oldest = timestamp; //Initialize to current timestamp, older should be older than this
  int oldestSlot = 0;
  
  for (int i = 0 ; i < GetSlotCount() ; i++) { //If not, find the oldest event and replace that
    if (eventList[i].timestamp < oldest) {
      oldest = eventList[i].timestamp;
      oldestSlot = i;
    }
  }
  
  if(oldest != timestamp){  //If an older slot was found (should always be the case)
    eventList[oldestSlot].event = id;
    eventList[oldestSlot].timestamp = timestamp;
    return 3;  //Replaced oldest existing other event
  }

  return 0;

}

int EventList::GetSlotCount() {
  return 8;
}

EventList::event EventList::GetEventAtSlot(int slot) {

  if (InRange(slot)) {
    return eventList[slot];
  }
  else {
    return { -1, 0};
  }

}

bool EventList::InRange(int slot) {

  if (slot < 0 || slot >= GetSlotCount()) { //Out of range
    return false;
  }
  else {
    return true;
  }
}

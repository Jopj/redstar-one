#include <Arduino.h>

#ifndef TemeriPID_h
#define TemeriPID_h

class TemeriPID{
  public:
    TemeriPID(const float *input, float *setpoint, float *p, float *i, float *d, float *lolim, float *hilim);
    float Compute(uint32_t now);
    void ResetI();
    void SetOutputLimits(float lolim, float hilim);
    void Active(bool active);
    void Initialize();
    float *kp, *ki, *kd;
    
  private:
    //short _pidActive;
    //short _newState;
    float *_hilim;
    float *_lolim;
    float _ITerm, _error, _dInput;		//Error is not static as it's computed every time again
    bool _active;  //When true, output is controlled. When false, I-term is reset
    
    const float *_input;
    float *_setpoint;
    //float _error;
    float _output;
    float _lastInput;				//This is needed to derivate input directly
    uint32_t _lastTime;
    
};

#endif

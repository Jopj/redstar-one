/*TODO

   Separate version of adafruit GFX library to stop conflicts
    over voltage event
    "footpad lifted at speed" event
    "Shutdown at speed" event - footpads and roll
    smoother velocity pushback
    Dynamic alert screen
    event readout with high accuracy
    Watthour drawn
*/




//#define CANON  //Enables CAN communications
//#define DEBUGPRINTOUT

#include "MPU9250_red.h"
//#include <MadgwickAHRS.h>  //Old version of madgwick with fixed integration time
#include "SensorFusion.h" //Madgwick & mahony algorithms
#include <EEPROM.h>
#include <VescUart.h>
#ifdef CANON
#include <FlexCAN.h>
#endif
#include "RedstarClasses.h"
#include "TemeriPID.h"

#include <i2c_t3.h>               //I2C for display
#include "src/Adafruit_GFX_Library/Adafruit_GFX.h"      //Graphics library
#include "src/Redstar_SSD1306/Redstar_SSD1306.h" //Modified Adafruit display driver
#include "src/Adafruit_GFX_Library/Fonts/FreeSansOblique24pt7b.h"
#include "src/Adafruit_GFX_Library/Fonts/FreeSansBoldOblique9pt7b.h"



//Pin definitions

const byte pcbLed = 30;
const byte tweeterPin = 29;

const byte segment1 = 36;
const byte segment2 = 35;
const byte segment3 = 8;
const byte segment4 = 7;
const byte segment5 = 6;
const byte segment6 = 5;
const byte segment7 = 2;
const byte segment8 = 37;
const byte segment9 = 38;
const byte segment10 = 14;

const byte uiR = 17;
const byte uiG = 22;
const byte uiB = 23;

const byte pwrLed = 16;

const byte btStatusPin = 32;
const byte uiBtn = 12;
const byte heelFootpadPin = A12;
const byte mainFootpadPin = A21;

//IMU 1
const byte imu1cs = 15;
const byte imu1int = 25;
const byte imu1sync = 26;

//IMU 2
const byte imu2cs = 20;
const byte imu2int = 24;
const byte imu2sync = 13;

//IMU 3
const byte imu3cs = 21;
const byte imu3int = 18;
const byte imu3sync = 19;

const byte canStandbyPin = 28;
//pin 34 CAN1RX
//pin 33 CAN1TX


int statusIMU1, statusIMU2, statusIMU3 = 0;
volatile bool newDataIMU1 = false;
volatile bool newDataIMU2 = false;
volatile bool newDataIMU3 = false;

volatile uint32_t intcount = 0;
volatile uint32_t intcount2 = 0;
volatile uint32_t intcount3 = 0;


const float todeg = 57.2957795f;
const float torad = 0.0174532925f;

float pidPitch = 0.0f;  //PID has pointer to this value, it should be updated whenever filter is. This is the PID input
float pidPitchTarget = 0.0f;  //Target pitch which PID tries to maintain. This is the PID setpoint
float pidOutputCurrent = 0.0f; //PID updates this value, it is always called but outputs 0 when inactive
bool pushbackActive = false;

int pmode = 0;


//============================== PARAMETERS=================================
//==========================================================================

const int cellCount = 12;   //Amount of cells in battery
//const float pushbackDuty = 0.75f; //When duty cycle is higher than this value, pushback is engaged. Vesc has max duty of 0.95.
const float pushbackDuty = 0.75f;
const float pushbackSpeedStart = 5.0f;
const float pushbackSpeedEnd = 10.0;

const float battLowLimit = 3.5f;  //Cell voltage limits to trigger beeping
const float battVeryLowLimit = 3.3f;

const bool motorTestBtn = false; //If true, UI button overrides footsensors and engages motor. Else, it does something else

float kp = 16.0f;  //PID tuning parameters for delta-wound 600w phub-188
float ki = 0.005f;
float kd = 0.0f;

//float kp = 14.0f;  //PID tuning parameters for delta-wound 600w phub-188
//float ki = 0.05f;
//float kd = 0.0f;

//float kp = 8.0f;  //PID tuning parameters for y-wound 600w phub-188
//float ki = 0.03f;
//float kd = 0.0f;

//"Alarm" level things cause pushback
const float batCurOutAlarm = 45.0f;  //Max allowed battery out current in VESC is 60
const float batCurInAlarm = -15.0f;   //Max allowed battery in current in vesc is 20
const float batCurInWarn = -12.0f;
const float motCurAlarm = 75.0f;
const float vescTempAlarm = 70.0f;
const float vescTempWarn = 65.0f;

float motMaxCur = 85.0f;  //Maximum and minimum current values commanded for motor. Should be equal, VESC must be set to allow at least this motor current
float motMinCur = -motMaxCur;

float battMaxCur = 60.0f;  //Max and min current for battery, these must be same as VESC settings to calculate effort accurately!
float battMinCur = -20.0f;

const float stopBrakeCurrent = 0.0f;      //Brake current in amps that is applied when in off mode (prevents rolloff)
const float readyBrakeCurrent = 15.0f;      //Brake current in amps that is applied when in "ready to drive" mode (helps getting up)
const float startAngleTolerance = 2.0f;   //Motor engages when board gets within this tolerance of level when starting
const float armingAngleTolerance = 6.0f;   //Board can arm when it gets within this tolerance of arming angle

const float pushbackAngle = 6.0f;        //Maximum pushback angle, pitch target will interpolate here when it is active (depending on direction
const float pushbackIncreaseRate = 8.0f;  //Increase rate (deg/s) in target angle when puchback activates
const float pushbackDecreaseRate = 5.0f; //Decrease rate (deg/s) of target angle when pushback deactivates
const float pushbackAngleTolerance = 0.5f;  //When target agle gets this close to the value it's following, it's just set there

float neutralAngle = 1.5f;  //pitch target will smoothly interpolate here when no pushback
const float armingAngle = 18.0f; // Board must be around this angle to arm, with some tolerance
const float cutoffInhibitRoll = 12.0f;  //At slow speed, power will not be cut if one footpad is not pressed at roll higher than this (turning tightly)

const int controlMicros = 10000; //Control loop delay rate, default 10000 us = 100Hz
const int imuMicros = 10000;      //IMU update rate
const int eepUpdateMs = 60000; //By default update eeprom every minute

const int heelFootpadLimH = 500;
const int heelFootpadLimL = 400;
const int mainFootpadLimH = 500;
const int mainFootpadLimL = 400;

const float clim = 0.2;  //Calibration limit value in deg/s, max sample variance must be within this to pass calibration
//const float diffLim = 3.0;  //IMU filter output total pitch + roll difference limit from eachother before the most different is discraded
const float aDiffLim = 0.75;  //Maximum allowed average gyro difference (m/s²) for an IMU (along all 3 axis) wrt the other ones before it is discarded
const float gDiffLim = 10.0f * torad; //Maximum allowed average gyro difference (deg/s)for an IMU (along all 3 axis) wrt the other ones before it is discarded


// ============================== EVENT TEXT ================================

const char * eventToText[] = {
  "-",                //Empty, 0 means no event
  "Duty %",        //1 - over duty cycle
  "Bat oC",       //2 - over battery current
  "Bat lo",        //3 - low battery voltage
  "Bat cr",      //4 - critical battery voltage
  "Mot oC",     //5 - Over motor current
  "Fet T",   //6 - over mosfet temp
  "VESC c",  //7 - Vesc communication error
  "IMU df", //8 - Imu difference
};


//static unsigned long vescFeedbackStamp = 0;
//static unsigned long dutyAlarmStamp = 0;          //This alarm triggers when motor duty cycle is getting too high
//static unsigned long battCurrentAlarmStamp = 0;   //This alarm triggers if battery current is close to max
//static unsigned long battCurrentWarnStamp = 0;    //This warning triggers when battery regen current is half of max
//static unsigned long battVoltageAlarmStamp = 0;
//static unsigned long motCurrentAlarmStamp = 0;   //This alarm triggers if motor current is close to max
//static unsigned long fetTempWarnStamp = 0;      //This warning triggers if VESC FET temperature is getting close to too high
//static unsigned long fetTempAlarmStamp = 0;    //This alarm triggers if VESC FET temperature is too high
//static unsigned long pushbackAlarmStamp = 0;    //This alarm triggers if VESC FET temperature is too high
//static unsigned long vescCommAlarmStamp = 0;    //This alarm triggers if VESC comm is not working (CRITICAL)

//============================ FLAGS AND THINGS =============================


bool heelFootpadPressed = false;
bool mainFootpadPressed = false;
uint32_t heelFootpadLiftedStamp = 0;  //Timestamp used for all footpad events
uint32_t mainFootpadLiftedStamp = 0;
uint32_t rollLastOkStamp = 0; //When roll angle is OK this is updated in control loop to current time

volatile unsigned int missedInts = 0;
int vescDisconnects = 0; //How many times vesc did not return data, should be 0 always..
int abnormalDelays = 0;  //If a loop takes more than 100 ms this gets incremented, should never happen

volatile uint32_t imu1ms = 0;
volatile uint32_t imu2ms = 0;
volatile uint32_t imu3ms = 0;
uint32_t loopms = 0;

struct CalibStruct {
  float xAccelBiasIMU1;
  float yAccelBiasIMU1;
  float zAccelBiasIMU1;
  float xAccelBiasIMU2;
  float yAccelBiasIMU2;
  float zAccelBiasIMU2;
  float xAccelBiasIMU3;
  float yAccelBiasIMU3;
  float zAccelBiasIMU3;
  uint64_t totalOdo;     //Total odometer in tics
  float maxFetTemp;
  float minFetTemp;
};

CalibStruct calibStruct = {0};

//Odometer related
const float wheeldiameter = 0.254;          //10 inch wheel
const float reductionration = 1;           //Direct drive hub motor
const int motorpoles = 30;                //30 magnetic poles in rotor
const int encoderPerRot = motorpoles * 3; //3 hall sensors
//This many meters per encoder tick
const double encToMeters = ((1.0 / (encoderPerRot)) / reductionration) * wheeldiameter * 3.14159;
//This many encder ticks per meter
const double metersToEnc = 1.0 / encToMeters;

uint64_t tripCompensator = 0; //What odom was at boot
uint64_t addedOdo = 0; //How many encoder tics have been added since previous odo update
int32_t unAddedOdo = 0; //Odo tics that have not yet been updated to eeprom

//Data output related
float maxBattVoltSeen = 0.0;
float minBattVoltSeen = 0.0;
float maxFetTempSeen = 0.0;
float minFetTempSeen = 0.0;

bool vescCommInitialized = false; //Flag to initialize vesc related variables when it connects
bool singleInit = true;  //Initialize some variables only once (if vesc disconnects)
bool screenDrive = false;  //Only write to screen if this is true, foes to false if init fails or when running there are i2c errors
bool nextScreen = false; //When true, screen toggles to next view

float cellVolts = 0.0;
float effort = 0.0;  //Percentage effort (by motor amp, batt amp + duty cycle limit, whichever is higher)
float effortMot = 0.0f;  //The components of effort
float effortBat = 0.0f;
float effortDuty = 0.0f;
float currentVelKmh = 0.0f;
bool goingForwards = true; //Saves current direction of movement so vesc struct needs not be accessed all the time

int leftNow = 0; //Footpad values, updated in footpad checker
int rightNow = 0;

float ax1, ay1, az1;
float gx1, gy1, gz1;

float ax2, ay2, az2;
float gx2, gy2, gz2;

float ax3, ay3, az3;
float gx3, gy3, gz3;

float gxA, gyA, gzA, axA, ayA, azA;  //Averaged gyro and accel values
float pitchA, rollA, yawA;  //Averaged rpy

float diff1, diff2, diff3;

float gDiff1, gDiff2, gDiff3;
float aDiff1, aDiff2, aDiff3;

float gDiffMax, aDiffMax;
int discardimu = 0; //Number of currently discarded imu, 0 means all are good
int lastDiscarded = 0; //Remembers which one it was for debugging
int lastDiscardedReason = 0;  // 0 = none, 1 = gyro, 2= accel

float imu1dt, imu1dtmax;

uint32_t lastImuDiscardStamp = 0;
uint32_t screenChangeStamp = 0;
uint32_t largestControlDelta = 0;


static const uint8_t logo[] = {

  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x98, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c, 0x9e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79, 0xcf, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xf1, 0xc7, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xe3, 0xe3, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x83, 0xe0, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x03, 0xe0, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x07, 0xf0, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1c, 0x07, 0xf0, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x3f, 0xff, 0xff, 0xfe, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff,
  0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x1f, 0xff, 0xff, 0xfc, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff,
  0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0x87, 0xff, 0xff, 0xf0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe,
  0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc1, 0xff, 0xff, 0xe1, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc,
  0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xe0, 0xff, 0xff, 0x83, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x00,
  0x00, 0x00, 0x07, 0xff, 0xff, 0xff, 0xf8, 0x3f, 0xfe, 0x0f, 0xff, 0xff, 0xff, 0xf0, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xfc, 0x7f, 0xff, 0x1f, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xfe, 0x7f, 0xff, 0x3f, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7e, 0x7f, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c, 0xff, 0xff, 0x9e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c, 0xfe, 0x3f, 0x9e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0xfc, 0x1f, 0x8c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0xf0, 0x07, 0xc8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xe0, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x80, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

//====================== OBJECTS ==========================

MPU9250 IMU_1(SPI, imu1cs);
MPU9250 IMU_2(SPI, imu2cs);
MPU9250 IMU_3(SPI, imu3cs);

//Madgwick filter;
//Madgwick filter2;
//Madgwick filter3;
//Madgwick filterA;

SF filter(0.1);  //Initialize with beta value
SF filter2(0.1);
SF filter3(0.1);
SF filterA(0.1);

VescUart VESC;

//input-setpoint-kp-ki-kd-minout-maxout
TemeriPID drivePID(&pidPitch, &pidPitchTarget, &kp, &ki, &kd, &motMinCur, &motMaxCur);

Beeperer beeper(tweeterPin);

//width, height, i2c bus, reset button (none) speed during and after transaction
Redstar_SSD1306 display(128, 32, &Wire2, -1, 800000UL, 800000UL);

EventList eventList(8);

#ifdef CANON
static CAN_message_t msg;
#endif

typedef void (*FunctionPointer) (void);

void ControlOff();
void ScreenKmh();
//void ScreenTemp();

FunctionPointer ControlFunction = ControlOff;                   //Start with parameter list
FunctionPointer ScreenFunction = ScreenKmh;
//FunctionPointer ScreenFunction = ScreenTemp;  //For testing

byte machineState = 0;  //State of the driving statemachine, 0=off, 1=armed,2=driving

void setup() {
  pinMode(pcbLed, OUTPUT);
  pinMode(tweeterPin, OUTPUT);

  pinMode(segment1, OUTPUT);
  pinMode(segment2, OUTPUT);
  pinMode(segment3, OUTPUT);
  pinMode(segment4, OUTPUT);
  pinMode(segment5, OUTPUT);
  pinMode(segment6, OUTPUT);
  pinMode(segment7, OUTPUT);
  pinMode(segment8, OUTPUT);
  pinMode(segment9, OUTPUT);
  pinMode(segment10, OUTPUT);

  pinMode(pwrLed, OUTPUT);

  pinMode(uiR, OUTPUT);
  pinMode(uiG, OUTPUT);
  pinMode(uiB, OUTPUT);

  pinMode(uiBtn, INPUT);
  pinMode(heelFootpadPin, INPUT);
  pinMode(mainFootpadPin, INPUT);

  pinMode(btStatusPin, INPUT);



  //digitalWrite(pcbLed, HIGH);
  digitalWrite(pwrLed, HIGH);

  analogWriteFrequency(tweeterPin, 2731);

  SPI.setMISO(39);  //Set SPI pins
  SPI.setSCK(27);

  Serial.begin(115200);  //Usb serial, baud doesn't actually matter
  Serial1.begin(115200);  //Vesc serial interface

  //Serial2.begin(38400);   //Bluetooth radio

#ifdef CANON
  Can1.begin(50000);    //Canbus 1

  for(uint8_t i = 0; i++ ; i < NUM_MAILBOXES){  //Set mask of all mailboxes to block any but exact ID matches
    Can1.setMask(0xFFFFFFFF,i);
  }
  CAN_filter_t filther;
  filther.flags.remote = 0;
  filther.flags.extended = 0;
  filther.id = 0x20;
  Can1.setFilter(filther,0); //Receive the battery status message

  //Can1.setMask
#endif


  pinMode(canStandbyPin, OUTPUT);

#ifdef CANON
  digitalWrite(canStandbyPin, LOW);
#else
  digitalWrite(canStandbyPin, HIGH);
#endif

  //================== Initialize screen =================
#define WIRE_PINS   I2C_PINS_3_4
  Wire2.begin(I2C_MASTER, 0x00, WIRE_PINS, I2C_PULLUP_EXT, 800000, I2C_OP_MODE_DMA);
  Wire2.setDefaultTimeout(10000); // 10ms

  //gen disp volt from 3.3, address, init i2c (done few linses up instead with DMA mode)
  screenDrive = display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false);  //this just checks if buffer is allocated
  //screenDrive = false;
  if (screenDrive) {
    display.fillScreen(0);          //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
    //    display.setCursor(0, 0);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    //    display.setTextColor(1);      //Tekstin väri
    //    display.setTextSize(3);      //Tekstin koko, perusfontin koon moninkertana
    //    display.print("REDSTAR"); //Kirjoita näytölle jotakin!
    //    display.display();
    //    display.setTextSize(1);


    display.drawBitmap(0, 0, logo, 128, 32, 1);
    display.display();
    display.setTextColor(1);
    display.setTextSize(1);
    display.setTextWrap(false);
  }

  //================== Initialize IMU 1 ==========================

  statusIMU1 = IMU_1.begin();
  if (statusIMU1 < 0) {
    Serial.println("IMU 1 initialization unsuccessful");
    Serial.print("Status: ");
    Serial.println(statusIMU1);
    while (1) {}
  }
  else {
    IMU_1.setSrd(9);  //samplerate divider from 1000hz (this + 1)
    IMU_1.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);  //Set low pass filtering on IMU
    IMU_1.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
    IMU_1.setAccelRange(MPU9250::ACCEL_RANGE_4G);
    //IMU_1.enableDataReadyInterrupt();
  }

  //================== Initialize IMU 2 ==========================
  statusIMU2 = IMU_2.begin();
  if (statusIMU2 < 0) {
    Serial.println("IMU 2 initialization unsuccessful");
    Serial.print("Status: ");
    Serial.println(statusIMU2);
    while (1) {}
  }
  else {
    IMU_2.setSrd(9);  //samplerate divider from 1000hz (this + 1)
    IMU_2.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);  //Set low pass filtering on IMU
    IMU_2.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
    IMU_2.setAccelRange(MPU9250::ACCEL_RANGE_4G);
    //IMU_2.enableDataReadyInterrupt();
  }

  //================== Initialize IMU 3 ==========================
  statusIMU3 = IMU_3.begin();
  if (statusIMU3 < 0) {
    Serial.println("IMU 3 initialization unsuccessful");
    Serial.print("Status: ");
    Serial.println(statusIMU3);
    while (1) {}
  }
  else {
    IMU_3.setSrd(9);  //samplerate divider from 1000hz (this + 1)
    IMU_3.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);  //Set low pass filtering on IMU
    IMU_3.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
    IMU_3.setAccelRange(MPU9250::ACCEL_RANGE_4G);
    //IMU_3.enableDataReadyInterrupt();
  }

  //attachInterrupt(imu1int, IMU1Int, RISING);
  //attachInterrupt(imu2int, IMU2Int, RISING);
  //attachInterrupt(imu3int, IMU3Int, RISING);

  //  filter.begin(100); //Initialize Madgwick filter with expected samplerate of 100 Hz
  //  filter2.begin(100); //Initialize Madgwick filter with expected samplerate of 100 Hz
  //  filter3.begin(100); //Initialize Madgwick filter with expected samplerate of 100 Hz
  //  filterA.begin(100); //Initialize Madgwick filter with expected samplerate of 100 Hz

  EEPROM.get(0, calibStruct);  //Get previous accelerometer calibration values from EEPROM

  tripCompensator = calibStruct.totalOdo;

  IMU_1.setAccelCalX(calibStruct.xAccelBiasIMU1, 1.0);  //Put the claibration values to use
  IMU_1.setAccelCalY(calibStruct.yAccelBiasIMU1, 1.0);
  IMU_1.setAccelCalZ(calibStruct.zAccelBiasIMU1, 1.0);

  IMU_2.setAccelCalX(calibStruct.xAccelBiasIMU2, 1.0);  //Put the claibration values to use
  IMU_2.setAccelCalY(calibStruct.yAccelBiasIMU2, 1.0);
  IMU_2.setAccelCalZ(calibStruct.zAccelBiasIMU2, 1.0);

  IMU_3.setAccelCalX(calibStruct.xAccelBiasIMU3, 1.0);  //Put the claibration values to use
  IMU_3.setAccelCalY(calibStruct.yAccelBiasIMU3, 1.0);
  IMU_3.setAccelCalZ(calibStruct.zAccelBiasIMU3, 1.0);

  Serial.println("Using the following accel biases from EEPROM");
  Serial.print("IMU1 x: ");
  Serial.print(calibStruct.xAccelBiasIMU1);
  Serial.print(" mss y: ");
  Serial.print(calibStruct.yAccelBiasIMU1);
  Serial.print(" mss z: ");
  Serial.print(calibStruct.zAccelBiasIMU1);
  Serial.println(" mss");

  Serial.print("IMU2 x: ");
  Serial.print(calibStruct.xAccelBiasIMU2);
  Serial.print(" mss y: ");
  Serial.print(calibStruct.yAccelBiasIMU2);
  Serial.print(" mss z: ");
  Serial.print(calibStruct.zAccelBiasIMU2);
  Serial.println(" mss");

  Serial.print("IMU3 x: ");
  Serial.print(calibStruct.xAccelBiasIMU3);
  Serial.print(" mss y: ");
  Serial.print(calibStruct.yAccelBiasIMU3);
  Serial.print(" mss z: ");
  Serial.print(calibStruct.zAccelBiasIMU3);
  Serial.println(" mss");

  VESC.setSerialPort(&Serial1);
  drivePID.Active(false);

  Serial.print("Last reset reason: ");
  printResetType(Serial);

  bool extendedCalibration = !digitalRead(uiBtn);

  Gyrocal(extendedCalibration);

  if (extendedCalibration) {
    Accelcal();   //This will compute new IMU accel biases
  }

  missedInts = 0;

  filter.setOrientation({0, 0.1564345, 0, 0.9876883});  //Set initial pose of filter to "17 degrees pitch up" which is the resting pose. Estimation begins here
  filter2.setOrientation({0, 0.1564345, 0, 0.9876883});
  filter3.setOrientation({0, 0.1564345, 0, 0.9876883});
  filterA.setOrientation({0, 0.1564345, 0, 0.9876883});

  beeper.AddToQueue(Beeperer::StartBeep);
}

void loop() {

  //  if (newDataIMU1) UpdateIMU1();  //These are readd in the control function now
  //  if (newDataIMU2) UpdateIMU2();
  //  if (newDataIMU3) UpdateIMU3();

  VescChecker();
  FootpadChecker();
  UpdateIMUs();
  Control();
  AudioHandler();
  EEPROMHandler();
  DebugPrintout(); //Prints debug stuff to serial if DEBUGPRINTOUT is defined
  //CanTest();
  CANGet();

  //EventGeneratorTest();
}

// ========================= SCREEN FUNCTIONS ============================


void MenuBar(int num) {
  int edges = 12;
  int spots = 11;
  int cog = (128 - edges) / spots;
  char ups[spots] =   {'V', 'B', 'W', 'B', 'O', 'W', 'E', 'L', 'E', 'D', 'T'};
  char downs[spots] = {'e', 'v', 't', 'a', 'd', 'h', 'v', 'r', '%', 'i', 'v'};

  display.setFont();
  display.fillTriangle(0, 24, 7, 17, 7, 31, 1);
  display.fillTriangle(127, 24, 120, 17, 120, 31, 1);
  for (int i = 0 ; i < spots ; i++) {
    if (num == i) {
      display.fillRect(edges + i * cog - 1, 18, 8, 18 , 1);
      display.setTextColor(0);
      display.setCursor(edges + i * cog, 18); display.print(ups[i]);
      display.setCursor(edges + i * cog, 25); display.print(downs[i]);
      display.setTextColor(1);
    }
    else {
      display.setCursor(edges + i * cog, 18); display.print(ups[i]);
      display.setCursor(edges + i * cog, 25); display.print(downs[i]);
    }
  }
}

void ScreenKmh() {  //Displays current speed in km/h


  //int kmhprint = abs((int)(((float)VESC.data.rpm*encToMeters/60.0f)*3.6f));
  //int kmhprint = abs((((float)VESC.data.rpm / ((float)motorpoles / 2.0f)) / 60.0f) * (float)wheeldiameter * 3.14159 * 3.6f);
  //int kmhprint = abs(VESC.data.avgMotorCurrent);
  int kmhprint = abs(currentVelKmh);
  //kmhprint = intcount;

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Velocity");
    MenuBar(0);
    display.display();
  }
  else {

    display.fillScreen(0);          //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu

    if (kmhprint < 10) display.setCursor(26, 31);    //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    else display.setCursor(-1, 31);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    display.setFont(&FreeSansOblique24pt7b);
    display.print(kmhprint); //Kirjoita näytölle jotakin!
    display.setCursor(64, 12);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.print("km/h"); //Kirjoita näytölle jotakin!
    display.display();
  }



  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenVolts;
  }
}

void ScreenVolts() {  //Displays battery voltage

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Battery volts");
    MenuBar(1);
    display.display();
  }
  else {

    display.setFont(&FreeSansOblique24pt7b);
    display.setCursor(0, 31);
    display.fillScreen(0);
    //display.print(VESC.data.inpVoltage, 2); //Kirjoita näytölle jotakin!
    display.print(cellVolts, 2); //Kirjoita näytölle jotakin!
    //display.print(pidPitchTarget, 2); //Kirjoita näytölle jotakin!
    display.display();        //Näytä muutokset näytöllä!
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenWatt;
  }
}
void ScreenWatt() {  //Displays motor current


  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Power W");
    MenuBar(2);
    display.display();
  }
  else {

    display.setFont(&FreeSansOblique24pt7b);
    int xpos = 94;
    int toPrint = (int)(VESC.data.avgInputCurrent * VESC.data.inpVoltage);
    //int toPrint = asd;
    if (abs(toPrint) > 10) { //Atleast 2 chars, push left
      xpos -= 25;
    }
    if (abs(toPrint) > 100) { //Atleast 3 chars, push left
      xpos -= 25;
    }
    if (abs(toPrint) > 1000) { //Atleast 4 chars, push left
      xpos -= 25;
    }
    if (toPrint < 0) { //Has minus, push left
      xpos -= 15;
    }
    display.setCursor(xpos, 31);
    display.fillScreen(0);
    display.print(toPrint); //Kirjoita näytölle jotakin!
    display.display();        //Näytä muutokset näytöllä!
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenBattA;
  }
}
void ScreenBattA() {  //Displays battery current

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Battery amps");
    MenuBar(3);
    display.display();
  }
  else {

    display.setFont(&FreeSansOblique24pt7b);
    int xpos = 0;
    float toPrint = VESC.data.avgInputCurrent;
    if (fabs(toPrint) < 10.0f) { //lacking  character, push back
      xpos += 25;
    }
    if (toPrint >= 0.0f) { //Lacking minus sign, push back
      xpos += 15;
    }
    display.setCursor(xpos, 31);
    display.fillScreen(0);
    display.print(toPrint, 1); //Kirjoita näytölle jotakin!
    display.display();        //Näytä muutokset näytöllä!
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenOdo;
  }
}

void ScreenOdo() {  //Displays trip and total odometer

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Odometer");
    MenuBar(4);
    display.display();
  }
  else {

    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);

    display.setCursor(0, 12);    //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    display.print("Trip: "); display.print(( (float)(calibStruct.totalOdo + unAddedOdo - tripCompensator) )*encToMeters, 2);
    display.setCursor(0, 30);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    display.print("Odo: "); display.print(( (float)(calibStruct.totalOdo + unAddedOdo) )*encToMeters, 0);
    display.display();        //Näytä muutokset näytöllä!
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenWattHour;
  }
}

void ScreenWattHour() {  //Displays drawn and regenerated mAh

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Wh In/Out");
    MenuBar(5);
    display.display();
  }
  else {

    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);

    display.setCursor(0, 12);    //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    display.print("Out: "); //display.print(VESC.data.ampHours * 1000.0f);
    display.print(VESC.data.wattHours, 2); //Kirjoita näytölle jotakin!
    //display.print(rightNow);
    display.setCursor(0, 30);     //Kursorin paikka, tekstin vasen yläkulma alkaa tästä
    display.print("In: "); //display.print(VESC.data.ampHoursCharged * 1000.0f);
    display.print(VESC.data.wattHoursCharged, 2); //Kirjoita näytölle jotakin!
    //display.print(leftNow);
    //display.setFont();
    display.setCursor(110, 24);
    display.print(pmode);
    display.display();        //Näytä muutokset näytöllä!
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenEvents;
  }
}

void ScreenEvents() {  //Displays list of occured events and time in seconds since they last happened

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Event list");
    MenuBar(6);
    display.display();
  }
  else {

    display.fillScreen(0);
    display.setFont();
    display.drawFastVLine(63, 0, 32, 1);


    if (eventList.GetEventAtSlot(0).event > 0) {
      display.setCursor(0, 0); display.print(Ep(eventList.GetEventAtSlot(0).event));
      display.setCursor(40, 0); display.print((millis() - eventList.GetEventAtSlot(0).timestamp) / 100);
    }


    if (eventList.GetEventAtSlot(1).event > 0) {
      display.setCursor(0, 8); display.print(Ep(eventList.GetEventAtSlot(1).event));
      display.setCursor(40, 8); display.print((millis() - eventList.GetEventAtSlot(1).timestamp) / 100);
    }


    if (eventList.GetEventAtSlot(2).event > 0) {
      display.setCursor(0, 16); display.print(Ep(eventList.GetEventAtSlot(2).event));
      display.setCursor(40, 16); display.print((millis() - eventList.GetEventAtSlot(2).timestamp) / 100);
    }


    if (eventList.GetEventAtSlot(3).event > 0) {
      display.setCursor(0, 24); display.print(Ep(eventList.GetEventAtSlot(3).event));
      display.setCursor(40, 24); display.print((millis() - eventList.GetEventAtSlot(3).timestamp) / 100);
    }


    if (eventList.GetEventAtSlot(4).event > 0) {
      display.setCursor(65, 0); display.print(Ep(eventList.GetEventAtSlot(4).event));
      display.setCursor(105, 0); display.print((millis() - eventList.GetEventAtSlot(4).timestamp) / 100);
    }


    if (eventList.GetEventAtSlot(5).event > 0) {
      display.setCursor(65, 8); display.print(Ep(eventList.GetEventAtSlot(5).event));
      display.setCursor(105, 8); display.print((millis() - eventList.GetEventAtSlot(5).timestamp) / 100);
    }


    if (eventList.GetEventAtSlot(6).event > 0) {
      display.setCursor(65, 16); display.print(Ep(eventList.GetEventAtSlot(6).event));
      display.setCursor(105, 16); display.print((millis() - eventList.GetEventAtSlot(6).timestamp) / 100);
    }


    if (eventList.GetEventAtSlot(7).event > 0) {
      display.setCursor(65, 24); display.print(Ep(eventList.GetEventAtSlot(7).event));
      display.setCursor(105, 24); display.print((millis() - eventList.GetEventAtSlot(7).timestamp) / 100);
    }

    display.display();        //Näytä muutokset näytöllä!
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenIntervals;
  }
}

void ScreenIntervals() {  //Displays the different loop intervals

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Loop rates");
    MenuBar(7);
    display.display();
  }
  else {

    display.setFont();
    display.fillScreen(0);          //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu

    display.setCursor(0, 0); display.print("imu1: "); display.print(imu1ms);
    display.setCursor(0, 8); display.print("imu2: "); display.print(imu2ms);
    display.setCursor(0, 16); display.print("imu3: "); display.print(imu3ms);
    display.setCursor(0, 24); display.print("loop: "); display.print(loopms);

    display.setCursor(72, 0); display.print("p1: "); display.print(filter.getPitch(), 1);
    display.setCursor(72, 8); display.print("p2: "); display.print(filter2.getPitch(), 1);
    display.setCursor(72, 16); display.print("p3: "); display.print(filter3.getPitch(), 1);
    display.setCursor(72, 24); display.print("pA: "); display.print(pitchA, 1);

    display.display();        //Näytä muutokset näytöllä!
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenEffort;
  }
}

void ScreenEffort() {

  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Effort percent");
    MenuBar(8);
    display.display();
  }
  else {
    char whichEffort;
    if (effortBat > effortMot) {
      if (effortDuty > effortBat) {
        whichEffort = 'D';
      }
      else {
        whichEffort = 'B';
      }
    }
    else {
      if (effortDuty > effortMot) {
        whichEffort = 'D';
      }
      else {
        whichEffort = 'M';
      }
    }

    display.setFont(&FreeSansOblique24pt7b);
    int xpos = 20;
    float toPrint = effort;
    if (fabs(toPrint) < 99.9f && fabs(toPrint >= 10.0f)) { //2 char
      xpos += 25;
    }
    else if (fabs(toPrint) < 10.0f) { // 1 char
      xpos += 50;
    }
    else {
      xpos += 0;   //2 char
    }
    display.setCursor(xpos, 31);
    display.fillScreen(0);
    display.print(toPrint, 0); //Kirjoita näytölle jotakin!
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(105, 12);
    display.print('%');
    display.setCursor(105, 30);
    display.print(whichEffort);
    display.display();        //Näytä muutokset näytöllä!


  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenImudiscard;
  }
}

//void ScreenFilters() {  //Displays pitch estimates of different filters, IMU differences and discarded imu if any
//
//  if (millis() - screenChangeStamp < 1000) {
//    display.fillScreen(0);
//    display.setFont(&FreeSansBoldOblique9pt7b);
//    display.setCursor(0, 12);
//    display.print("Filter pitches +");
//    MenuBar(8);
//    display.display();
//  }
//  else {
//
//    display.setFont();
//    display.fillScreen(0);          //Täytä näyttö Adafruit-logon poistamiseksi, 0 on pimeä ja 1 valaistu
//
//    display.setCursor(0, 0); display.print("p1: "); display.print(filter.getPitch(), 1);
//    display.setCursor(0, 8); display.print("p2: "); display.print(filter2.getPitch(), 1);
//    display.setCursor(0, 16); display.print("p3: "); display.print(filter3.getPitch(), 1);
//    display.setCursor(0, 24); display.print("pA: "); display.print(pitchA, 1);
//
//    display.setCursor(72, 0); display.print("id: "); display.print(discardimu);
//    display.setCursor(72, 8); display.print("d1: "); display.print(diff1, 2);
//    display.setCursor(72, 16); display.print("d2: "); display.print(diff2, 2);
//    display.setCursor(72, 24); display.print("d3: "); display.print(diff3, 2);
//
//    display.display();        //Näytä muutokset näytöllä!
//  }
//
//  if (nextScreen) {
//    nextScreen = false;
//    screenChangeStamp = millis();
//    ScreenFunction = ScreenImudiscard;
//  }
//}

void ScreenImudiscard() {  //Displays currently discarded IMU, time from w



  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Discarded IMU");
    MenuBar(9);
    display.display();
  }
  else {

    display.fillScreen(0);
    //    display.setFont(&FreeSansOblique24pt7b);
    //    display.setCursor(5, 31);
    //    display.print(lastDiscarded);
    //    display.setFont();
    //    display.setCursor(44, 0);
    //    display.setTextSize(2);
    //    display.print((millis() - lastImuDiscardStamp) / 1000);
    //    display.setCursor(44, 16);
    //    display.print(millis() / 1000);
    //    display.setFont();
    //    display.setTextSize(1);
    //    display.setCursor(103,0); display.print(diff1);
    //    display.setCursor(103,8); display.print(diff2);
    //    display.setCursor(103,16); display.print(diff3);
    //    display.setCursor(103,24); display.print(maxDiff);
    display.setFont();
    display.setTextSize(1);
    display.setCursor(0, 0); display.print("g1:"); display.print(gDiff1 * todeg, 2);
    display.setCursor(0, 8); display.print("g2:"); display.print(gDiff2 * todeg, 2);
    display.setCursor(0, 16); display.print("g3:"); display.print(gDiff3 * todeg, 2);
    display.setCursor(0, 24); display.print("gd:"); display.print(gDiffMax * todeg, 2);

    display.setCursor(48, 0); display.print("a1:"); display.print(aDiff1, 2);
    display.setCursor(48, 8); display.print("a2:"); display.print(aDiff2, 2);
    display.setCursor(48, 16); display.print("a3:"); display.print(aDiff3, 2);
    display.setCursor(48, 24); display.print("ad:"); display.print(aDiffMax, 2);

    display.setCursor(96, 0); display.print("ld: "); display.print(lastDiscarded);
    display.setCursor(96, 8); display.print("di: "); display.print(discardimu);
    display.setCursor(96, 16); display.print("re: "); display.print(lastDiscardedReason);
    display.setCursor(96, 24); display.print((millis() - lastImuDiscardStamp) / 1000);
    display.display();

  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenTemp;
  }
}

void ScreenTemp() {  //Displays currently discarded IMU, time from when one was last discarded + trip odometer


  if (millis() - screenChangeStamp < 1000) {
    display.fillScreen(0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.setCursor(0, 12);
    display.print("Temperatures");
    MenuBar(10);
    display.display();
  }
  else {
    char tempIdChar = 'Y';
    char tempIdCharCandidate1 = 'X';
    char tempIdCharCandidate2 = 'X';
    float tempToPrint = -3.2f;
    float tempCand1 = -2.2f;
    float tempCand2 = -2.3f;
    float tempVesc = VESC.data.temperatureFet;
    float tempMot = VESC.data.temperatureMotor;
    float tempBMS = -9.3f;
    float tempCell = -9.4f;

    //Comparison between motor and vesc temp, round 1
    if (tempVesc > tempMot) {
      tempIdCharCandidate1 = 'V';
      tempCand1 = tempVesc;
    }
    else {
      tempIdCharCandidate1 = 'M';
      tempCand1 = tempMot;
    }
    //Comparison between BMS and cell temp, round 1
    if (tempBMS > tempCell) {
      tempIdCharCandidate2 = 'B';
      tempCand2 = tempBMS;
    }
    else {
      tempIdCharCandidate2 = 'C';
      tempCand2 = tempCell;
    }
    //Comparison between vesc/mot and bms/cell temps, final round
    if (tempCand1 > tempCand2) {
      tempIdChar = tempIdCharCandidate1;
      tempToPrint = tempCand1;
    }
    else {
      tempIdChar = tempIdCharCandidate2;
      tempToPrint = tempCand2;
    }



    display.setFont(&FreeSansOblique24pt7b);
    int xpos = 0;

    if (fabs(tempToPrint) < 10.0f) { //lacking  character, push back
      xpos += 20;
    }
    if (tempToPrint >= 0.0f) { //Lacking minus sign, push back
      xpos += 10;
    }
    display.setCursor(xpos, 31);
    display.fillScreen(0);
    display.print(tempToPrint, 0);
    display.setFont(&FreeSansBoldOblique9pt7b);
    display.print(tempIdChar);
    display.setCursor(display.getCursorX() - 8, 15);
    display.print('C');

    display.setFont();
    display.setCursor(88, 0); display.print("V:"); display.print(tempVesc, 1);
    display.setCursor(88, 8); display.print("M:"); display.print(tempMot, 1);
    display.setCursor(88, 16); display.print("B:"); display.print(tempBMS, 1);
    display.setCursor(88, 24); display.print("C:"); display.print(tempCell, 1);


    display.display();
  }

  if (nextScreen) {
    nextScreen = false;
    screenChangeStamp = millis();
    ScreenFunction = ScreenKmh;
  }
}



//Run whichever screen is pointed to by "ScreenFunction" every x ms, if there have been no I2C errors.
void UpdateScreen() {
  static uint32_t screenStamp = 0;
  if (screenDrive && (millis() - screenStamp >= 50)) {
    screenStamp = millis();
    ScreenFunction();
    screenDrive = i2c_check();
  }
}

bool i2c_check() {
  bool allOk = true;
  int errors = Wire2.getErrorCount(I2C_ERRCNT_RESET_BUS) + Wire2.getErrorCount(I2C_ERRCNT_TIMEOUT) + Wire2.getErrorCount(I2C_ERRCNT_ADDR_NAK) + Wire2.getErrorCount(I2C_ERRCNT_DATA_NAK)
               + Wire2.getErrorCount(I2C_ERRCNT_ARBL) + Wire2.getErrorCount(I2C_ERRCNT_NOT_ACQ) + Wire2.getErrorCount(I2C_ERRCNT_DMA_ERR);
  if (errors > 1) {
    allOk = false; //Turn off flag to stop trying to talk to display
  }
  return allOk;
}

//void BluetoothHandler() {
//  static bool btOn = false;
//  static bool amPrinting = false;
//  bool btNow = digitalRead(btStatusPin);
//
//  if (!btOn && btNow) { //Bluetooth connected just now
//    beeper.AddToQueue(Beeperer::BluetoothConnect);
//    btOn = true;
//  }
//  else if (btOn && !btNow) { //Bluetooth disconnected just now, beep
//    beeper.AddToQueue(Beeperer::BluetoothDisconnect);
//    btOn = false;
//  }
//
//  if (btNow) {
//    if (amPrinting) {
//      if (Serial2.available()) {
//        char dump = Serial2.read();  //Clear buffer one loop at a time
//      }
//      else {
//        amPrinting = false;
//      }
//    }
//    else {
//      if (Serial2.available()) {
//        amPrinting = true;
//        if (machineState == 0) {
//          InfoDump(Serial2);  //Dump info to BT once if anything is received
//        }
//
//        beeper.AddToQueue(Beeperer::Bip);
//      }
//    }
//  }
//}

void CanTest() {
#ifdef CANON
  static uint32_t canStamp = 0;
  static uint64_t inc = 0;
  if (micros() - canStamp > 1000) {

    canStamp = micros();

    msg.ext = 0;
    msg.id = 0x100;
    msg.len = 8;

    msg.buf[0] = inc;
    msg.buf[1] = inc >> 8;
    msg.buf[2] = inc >> 16;
    msg.buf[3] = inc >> 24;
    msg.buf[4] = inc >> 32;
    msg.buf[5] = inc >> 40;
    msg.buf[6] = inc >> 48;
    msg.buf[7] = (uint8_t)imu1ms;//inc>>56;

    Can1.write(msg);
    inc++;
  }
#endif
}

void CANGet() {
#ifdef CANON
  CAN_message_t inMsg;
  if (Can1.available())
  {
    Can1.read(inMsg);
    intcount++;
  }
#endif
}

void EEPROMHandler() {
  static uint32_t eepStamp = 0;
  bool eepSaveNow = false;

  if (millis() - eepStamp > eepUpdateMs) {
    eepStamp = millis();
    if (unAddedOdo > 10) {
      calibStruct.totalOdo += unAddedOdo;
      addedOdo += unAddedOdo;
      unAddedOdo = 0;
      eepSaveNow = true;
    }
    if (maxFetTempSeen > calibStruct.maxFetTemp) {
      calibStruct.maxFetTemp = maxFetTempSeen;
      eepSaveNow = true;
    }
    if (minFetTempSeen < calibStruct.minFetTemp) {
      calibStruct.minFetTemp = minFetTempSeen;
      eepSaveNow = true;
    }

    if (eepSaveNow) {
      EEPROM.put(0, calibStruct);
    }

  }
}

void VescChecker() {
  static uint32_t vescFeedbackStamp = 0;
  static uint32_t dutyAlarmStamp = 0;          //This alarm triggers when motor duty cycle is getting too high
  static uint32_t battCurrentAlarmStamp = 0;   //This alarm triggers if battery current is close to max
  static uint32_t battCurrentWarnStamp = 0;    //This warning triggers when battery regen current is half of max
  static uint32_t battVoltageAlarmStamp = 0;
  static uint32_t motCurrentAlarmStamp = 0;   //This alarm triggers if motor current is close to max
  static uint32_t fetTempWarnStamp = 0;      //This warning triggers if VESC FET temperature is getting close to too high
  static uint32_t fetTempAlarmStamp = 0;    //This alarm triggers if VESC FET temperature is too high
  static uint32_t vescCommAlarmStamp = 0;    //This alarm triggers if VESC comm is not working (CRITICAL)

  static long odoAbsifier = 0;


  if (micros() - vescFeedbackStamp > 50000) {
    vescFeedbackStamp = micros();
    if ( VESC.getVescValues() ) {

      if (!vescCommInitialized) {
        vescCommInitialized = true;
        maxFetTempSeen = VESC.data.temperatureFet;
        minFetTempSeen = VESC.data.temperatureFet;

        //addedOdo = VESC.data.tachometerAbs; //Do not add any of old vesc values, these should already be added
        odoAbsifier = VESC.data.tachometer;
        //addedOdo  =
        if (singleInit) {
          singleInit = false;
          maxBattVoltSeen = VESC.data.inpVoltage;
          minBattVoltSeen = VESC.data.inpVoltage;
        }
      }

      //Record tachometer
      long odiff = VESC.data.tachometer - odoAbsifier;
      unAddedOdo += abs(odiff);
      odoAbsifier = VESC.data.tachometer;

      //Record max and min voltages
      if (VESC.data.inpVoltage > maxBattVoltSeen) maxBattVoltSeen = VESC.data.inpVoltage;
      if (VESC.data.inpVoltage < minBattVoltSeen) minBattVoltSeen = VESC.data.inpVoltage;

      //Record max and min fet temps
      if (VESC.data.temperatureFet > maxFetTempSeen) maxFetTempSeen = VESC.data.temperatureFet;
      if (VESC.data.temperatureFet < minFetTempSeen) minFetTempSeen = VESC.data.temperatureFet;

      cellVolts = VESC.data.inpVoltage / cellCount;

      if (machineState == 0) {  // When not running, use UI controls (includes voltage display, changing of OLED function
        UiFunctions();

      }
      else {   //When running, zero the led bar. OLED is updated
        SegmentFiller(0);
      }

      //  ==== Current velocity ======

      currentVelKmh = (((float)VESC.data.rpm / ((float)motorpoles / 2.0f)) / 60.0f) * (float)wheeldiameter * 3.14159 * 3.6f;
      goingForwards = VESC.data.rpm > 0;

      //  ==== BATTERY VOLTAGE CHECK =====

      if (cellVolts < battLowLimit) {
        if (cellVolts < battVeryLowLimit) {
          if (millis() - battVoltageAlarmStamp > 5000) {
            battVoltageAlarmStamp = millis();
            beeper.AddToQueue(Beeperer::BattVeryLow);
            eventList.PushBack(4, millis());  //Battery critical event
          }
        }
        else {
          if (millis() - battVoltageAlarmStamp > 10000) {
            battVoltageAlarmStamp = millis();
            beeper.AddToQueue(Beeperer::BattLow);
            eventList.PushBack(3, millis());  //Battery low event
          }
        }
      }

      // ================ CALCULATE EFFORT =============



      float highest = 0.0f;

      // Motor effort, it is actually symmetric both in and out
      if (VESC.data.avgMotorCurrent > 0.0f) {
        effortMot = fmap(VESC.data.avgMotorCurrent, 0.0f, motMaxCur, 0.0f, 100.0f);
      }
      else {
        effortMot = fmap(VESC.data.avgMotorCurrent, 0.0f, motMinCur, 0.0f, 100.0f);
      }

      //Battery effort, not symmetric
      if (VESC.data.avgInputCurrent > 0.0f) {
        effortBat = fmap(VESC.data.avgInputCurrent, 0.0f, battMaxCur, 0.0f, 100.0f);
      }
      else {
        effortBat = fmap(VESC.data.avgInputCurrent, 0.0f, battMinCur, 0.0f, 100.0f);
      }

      //Duty cycle effort, maximum duty is 0.95
      effortDuty = fmap(fabs(VESC.data.dutyCycleNow), 0.0f, 0.95f, 0.0f, 100.0f);

      //Find out which component is highest, that is the overall (limiting) effort value
      highest = (effortMot > effortBat) ? effortMot : effortBat;
      highest = (highest > effortDuty) ? highest : effortDuty;
      effort = highest;


      // =================== PUSHBACK =====================

      bool pushbackNeeded = false;

      if (fabs(VESC.data.dutyCycleNow) > pushbackDuty) {  //overspeed!
        pushbackNeeded = true;
        if (millis() - dutyAlarmStamp > 500) {   //Keep adding beeps
          dutyAlarmStamp = millis();
          beeper.AddToQueue(Beeperer::Overspeed);
          eventList.PushBack(1, millis());  //Over duty cycle event
        }
      }

      if ((VESC.data.avgInputCurrent > batCurOutAlarm || VESC.data.avgInputCurrent < batCurInAlarm)) {  //Over battery current!
        pushbackNeeded = true;
        if (millis() - battCurrentAlarmStamp > 500) {
          battCurrentAlarmStamp = millis();
          beeper.AddToQueue(Beeperer::OvercurrentBat);
          eventList.PushBack(2, millis());  //Battery critical event
        }
      }

      if (VESC.data.avgInputCurrent < batCurInWarn) { //High regen current warn!
        if (millis() - battCurrentWarnStamp > 500) {
          battCurrentWarnStamp = millis();
          beeper.AddToQueue(Beeperer::RegencurrentWarn);
        }
      }

      if (fabs(VESC.data.avgMotorCurrent) > motCurAlarm) {   //Over motor current!
        pushbackNeeded = true;
        if (millis() - motCurrentAlarmStamp > 500) {
          motCurrentAlarmStamp = millis();
          beeper.AddToQueue(Beeperer::OvercurrentMot);
          eventList.PushBack(5, millis());  //Over motor current event
        }
      }

      if (fabs(VESC.data.temperatureFet) > vescTempWarn) {   //Wamr VESC fets! No pushback yet
        if (millis() - fetTempWarnStamp > 4000) {
          fetTempWarnStamp = millis();
          beeper.AddToQueue(Beeperer::WarmtempFet);
        }
      }

      if (fabs(VESC.data.temperatureFet) > vescTempAlarm) {   //Overtemp on VESC fets!
        pushbackNeeded = true;
        if (millis() - fetTempAlarmStamp > 500) {
          fetTempAlarmStamp = millis();
          beeper.AddToQueue(Beeperer::OvertempFet);
          eventList.PushBack(6, millis());  //Over mosfet temp event
        }
      }
      pushbackActive = pushbackNeeded;


    }
    else {
      vescCommInitialized = false;
      if (!singleInit) vescDisconnects++;; //If vesc has connected, record any disconnects.
      if (millis() - vescCommAlarmStamp > 1000) {
        vescCommAlarmStamp = millis();
        beeper.AddToQueue(Beeperer::Critical);
        eventList.PushBack(7, millis());  //Vesc comm error event
      }
    }
  }
}

void ButtonColors() {
  static float blueOut = 0.0;
  static float sonne = 0.0;

  sonne += 0.1f;

  //analogWrite(pcbLed, ((abs(pitch) > 0.5) ? map(abs(pitch), 0, 90, 0, 255) : 0));
  analogWrite(uiG, ((fabs(rollA) > 2) ? map(fabs(rollA), 2, 90, 0, 255) : 0));
  analogWrite(uiR, ((fabs(pitchA) > 2) ? map(fabs(pitchA), 2, 90, 0, 255) : 0));

  if (!digitalRead(uiBtn)) {
    blueOut = 255.0f;
    //beeper.AddToQueue(Beeperer::ImuDiff);
  }
  else {
  }

  blueOut = max(0.0, blueOut - 4.0);
  analogWrite(uiB, blueOut);

  if (cellVolts < battLowLimit) {
    analogWrite(pwrLed, (sinf(sonne) + 1.0f) * 120.0f + 15.0f);
  }
  else {
    digitalWrite(pwrLed, HIGH);
  }

}

void IMUAverage() {
  static unsigned int firstZero = 0;
  //static uint32_t lastBeeped_disc = 0;
  float alpha = 0.01f;
  float alpha2 = 0.05f;


  discardimu = 0;  //Number of currently discarded IMU, 0 means all good

  //  //Difference of IMUX to the other two, combined roll + pitch
  //  diff1 = fabs( ((filter2.getPitch() + filter3.getPitch()) / 2.0f) - filter.getPitch() ); + fabs( ((filter2.getRoll() + filter3.getRoll()) / 2.0f) - filter.getRoll() );
  //  diff2 = fabs( ((filter.getPitch() + filter3.getPitch()) / 2.0f) - filter2.getPitch() ); + fabs( ((filter.getRoll() + filter3.getRoll()) / 2.0f) - filter2.getRoll() );
  //  diff3 = fabs( ((filter.getPitch() + filter2.getPitch()) / 2.0f) - filter3.getPitch() ); + fabs( ((filter.getRoll() + filter2.getRoll()) / 2.0f) - filter3.getRoll() );

  gDiff1 = (fabs( ((gx2 + gx3) / 2.0f) - gx1 ) +  fabs( ((gy2 + gy3) / 2.0f) - gy1 ) + fabs( ((gz2 + gz3) / 2.0f) - gz1 )) * alpha + (1.0f - alpha) * gDiff1;
  gDiff2 = (fabs( ((gx1 + gx3) / 2.0f) - gx2 ) +  fabs( ((gy1 + gy3) / 2.0f) - gy2 ) + fabs( ((gz1 + gz3) / 2.0f) - gz2 )) * alpha + (1.0f - alpha) * gDiff2;
  gDiff3 = (fabs( ((gx1 + gx2) / 2.0f) - gx3 ) +  fabs( ((gy1 + gy2) / 2.0f) - gy3 ) + fabs( ((gz1 + gz2) / 2.0f) - gz3 )) * alpha + (1.0f - alpha) * gDiff3;

  aDiff1 = (fabs( ((ax2 + ax3) / 2.0f) - ax1 ) +  fabs( ((ay2 + ay3) / 2.0f) - ay1 ) + fabs( ((az2 + az3) / 2.0f) - az1 )) * alpha2 + (1.0f - alpha2) * aDiff1;
  aDiff2 = (fabs( ((ax1 + ax3) / 2.0f) - ax2 ) +  fabs( ((ay1 + ay3) / 2.0f) - ay2 ) + fabs( ((az1 + az3) / 2.0f) - az2 )) * alpha2 + (1.0f - alpha2) * aDiff2;
  aDiff3 = (fabs( ((ax1 + ax2) / 2.0f) - ax3 ) +  fabs( ((ay1 + ay2) / 2.0f) - ay3 ) + fabs( ((az1 + az2) / 2.0f) - az3 )) * alpha2 + (1.0f - alpha2) * aDiff3;

  // ======= ACCEL DISCARD  =========

  //If gyro 1 difference is above limit and larger than the other two
  if (aDiff1 > aDiffLim && aDiff1 > aDiff2 && aDiff1 > aDiff3) {
    discardimu = 1;
    lastDiscardedReason = 2;
  }
  //If gyro 2 difference is above limit and larger than the other two
  if (aDiff2 > aDiffLim && aDiff2 > aDiff1 && aDiff2 > aDiff3) {
    discardimu = 2;
    lastDiscardedReason = 2;
  }
  //If gyro 3 difference is above limit and larger than the other two
  if (aDiff3 > aDiffLim && aDiff3 > aDiff1 && aDiff3 > aDiff2) {
    discardimu = 3;
    lastDiscardedReason = 2;
  }

  // ======= GYRO DISCARD  =========

  //If gyro 1 difference is above limit and larger than the other two
  if (gDiff1 > gDiffLim && gDiff1 > gDiff2 && gDiff1 > gDiff3) {
    discardimu = 1;
    lastDiscardedReason = 1;
  }
  //If gyro 2 difference is above limit and larger than the other two
  if (gDiff2 > gDiffLim && gDiff2 > gDiff1 && gDiff2 > gDiff3) {
    discardimu = 2;
    lastDiscardedReason = 1;
  }
  //If gyro 3 difference is above limit and larger than the other two
  if (gDiff3 > gDiffLim && gDiff3 > gDiff1 && gDiff3 > gDiff2) {
    discardimu = 3;
    lastDiscardedReason = 1;
  }


  if (gDiff1 > gDiffMax) gDiffMax = gDiff1;
  if (gDiff2 > gDiffMax) gDiffMax = gDiff2;
  if (gDiff3 > gDiffMax) gDiffMax = gDiff3;

  if (aDiff1 > aDiffMax) aDiffMax = aDiff1;
  if (aDiff2 > aDiffMax) aDiffMax = aDiff2;
  if (aDiff3 > aDiffMax) aDiffMax = aDiff3;

  if (firstZero < 50) { //For the first few loops, do not discard or record things to let the IMU's settle
    gDiffMax = 0.0; aDiffMax = 0.0;
    firstZero++;
    discardimu = 0;
  }

  //IMU1 is worst and over the limit, do not use it!
  if (discardimu == 1) {
    gxA = (gx2 + gx3) / 2.0f;
    gyA = (gy2 + gy3) / 2.0f;
    gzA = (gz2 + gz3) / 2.0f;
    axA = (ax2 + ax3) / 2.0f;
    ayA = (ay2 + ay3) / 2.0f;
    azA = (az2 + az3) / 2.0f;

    filterA.MadgwickUpdate(gxA, gyA, gzA, axA, ayA, azA, filterA.deltatUpdate());

    //    pitchA = (filter2.getPitch() + filter3.getPitch()) / 2.0f;
    //    rollA = (filter2.getRoll() + filter3.getRoll()) / 2.0f;
    //    yawA = (filter2.getYaw() + filter3.getYaw()) / 2.0f;
  }

  //IMU2 is worst and over the limit, do not use it!
  else if (discardimu == 2) {
    gxA = (gx1 + gx3) / 2.0f;
    gyA = (gy1 + gy3) / 2.0f;
    gzA = (gz1 + gz3) / 2.0f;
    axA = (ax1 + ax3) / 2.0f;
    ayA = (ay1 + ay3) / 2.0f;
    azA = (az1 + az3) / 2.0f;

    filterA.MadgwickUpdate(gxA, gyA, gzA, axA, ayA, azA, filterA.deltatUpdate());

    //    pitchA = ( filter.getPitch() + filter3.getPitch()) / 2.0f;
    //    rollA = (filter.getRoll() + filter3.getRoll()) / 2.0f;
    //    yawA = (filter.getYaw() + filter3.getYaw()) / 2.0f;
  }

  //IMU3 is worst and over the limit, do not use it!
  else if (discardimu == 3) {
    gxA = (gx1 + gx2) / 2.0f;
    gyA = (gy1 + gy2) / 2.0f;
    gzA = (gz1 + gz2) / 2.0f;
    axA = (ax1 + ax2) / 2.0f;
    ayA = (ay1 + ay2) / 2.0f;
    azA = (az1 + az2) / 2.0f;

    filterA.MadgwickUpdate(gxA, gyA, gzA, axA, ayA, azA, filterA.deltatUpdate());

    //    pitchA = ( filter.getPitch() + filter2.getPitch()) / 2.0f;
    //    rollA = (filter.getRoll() + filter2.getRoll()) / 2.0f;
    //    yawA = (filter.getYaw() + filter2.getYaw()) / 2.0f;
  }

  //All IMUs are close enough to eachother, use all of them!
  else {
    gxA = (gx1 + gx2 + gx3) / 3.0f;
    gyA = (gy1 + gy2 + gy3) / 3.0f;
    gzA = (gz1 + gz2 + gz3) / 3.0f;
    axA = (ax1 + ax2 + ax3) / 3.0f;
    ayA = (ay1 + ay2 + ay3) / 3.0f;
    azA = (az1 + az2 + az3) / 3.0f;

    filterA.MadgwickUpdate(gxA, gyA, gzA, axA, ayA, azA, filterA.deltatUpdate());

    //    pitchA = (filter.getPitch() + filter2.getPitch() + filter3.getPitch()) / 3.0f;
    //    rollA = (filter.getRoll() + filter2.getRoll() + filter3.getRoll()) / 3.0f;
    //    yawA = (filter.getYaw() + filter2.getYaw() + filter3.getYaw()) / 3.0f;
  }

  pitchA = filterA.getPitch();
  rollA = filterA.getRoll();
  yawA = filterA.getYaw();

  if (discardimu) {
    lastImuDiscardStamp = millis(); //Record time when IMU was discarded
    eventList.PushBack(8, millis());  //IMU discard event
    lastDiscarded = discardimu;
    //    if(millis() - lastBeeped_disc > 1500){
    //      lastBeeped_disc = millis();
    //      //beeper.AddToQueue(Beeperer::BluetoothDisconnect);
    //    }
  }
}

void UpdateIMUs() {
  static uint32_t imupdateStamp = 0;

  if (micros() - imupdateStamp >= imuMicros) {
    imupdateStamp = micros();

    UpdateIMU1();  //These are readd in the control function now
    UpdateIMU2();
    UpdateIMU3();
    IMUAverage();
  }
}

void Control() {
  static uint32_t controlStamp = 0;


  if (micros() - controlStamp >= controlMicros) {


    loopms = micros() - controlStamp;
    controlStamp = micros();

    if (loopms > largestControlDelta)
    {
      largestControlDelta = loopms;
    }

    if (loopms > 12000 && millis() > 10000) {
      abnormalDelays++;
    }

    ButtonColors();

    ControlFunction();

    UpdateScreen();
  }
}

void ControlOff() {
  machineState = 0;
  VESC.setCurrent(0.0);  //Turn motor off
  //VESC.setBrakeCurrent(stopBrakeCurrent);

  //If both pads are pressed and board is oriented right (or test button pressed), get ready
  if ((mainFootpadPressed && heelFootpadPressed && (fabs(pitchA - armingAngle) < armingAngleTolerance) ) || (motorTestBtn && !digitalRead(uiBtn))) {
    beeper.AddToQueue(Beeperer::ReadyMode);
    ControlFunction = ControlArmed;
  }

}
void ControlArmed() {
  pidPitchTarget = 0.0f;

  machineState = 1;
  //VESC.setCurrent(0.0);  //Turn motor off
  VESC.setBrakeCurrent(readyBrakeCurrent);

  if (motorTestBtn && !digitalRead(uiBtn)) {   //Straight to "on" mode if test button pressed
    ControlFunction = ControlOn;
    drivePID.Active(true); //Activate drive
  }
  else {  //Else use normal logic
    //If both pads are pressed still + angle goes around 0 degrees, go to drive mode
    if ((mainFootpadPressed && heelFootpadPressed && (fabs(pitchA - neutralAngle) < startAngleTolerance)) ) {
      beeper.AddToQueue(Beeperer::Arm);
      ControlFunction = ControlOn;
      drivePID.Active(true); //Activate drive PID
      pidPitchTarget = 0.0f;  //Reset this just in case
    }
    //If both pads are off, return to off state
    if (!heelFootpadPressed && (millis() - heelFootpadLiftedStamp) > 1000) {
      beeper.AddToQueue(Beeperer::Disarm);
      ControlFunction = ControlOff;
      drivePID.Active(false); //Deactivate drive PID
    }
    if (!mainFootpadPressed && (millis() - mainFootpadLiftedStamp) > 1000) {
      beeper.AddToQueue(Beeperer::Disarm);
      ControlFunction = ControlOff;
      drivePID.Active(false); //Deactivate drive PID
    }
  }


}
void ControlOn() {

  machineState = 2;

  pidPitch = pitchA;


  //============== PUSHBACK =================

  if (pushbackActive) { //This is determined in the VESC feedback function
    if (goingForwards) { //In case we are going forward, pushback pitches up (positive pitch)
      PitchTargetAdjuster(pidPitchTarget, pushbackAngle, 6.0);
      //      if (pidPitchTarget < (neutralAngle + pushbackAngle)) {
      //        pidPitchTarget += 0.08;
      //      }
      pmode = 1;
    }
    else {   //In case we are going backwards, pushback pitches dowsn (negative pitch)
      PitchTargetAdjuster(pidPitchTarget, -pushbackAngle, 6.0);
      //      if (pidPitchTarget > (neutralAngle - pushbackAngle)) {
      //        pidPitchTarget -= 0.08;
      //      }
      pmode = 2;
    }
  }
  else {
    if (fabs(currentVelKmh) < 3.0f) {
      PitchTargetAdjuster(pidPitchTarget, 0.0, 0.5); //Pull toward zero
      //    if (fabs(pidPitchTarget - neutralAngle) < 0.2) { //Close enough, set to neutral angle
      //      pidPitchTarget = neutralAngle;
      //    }
      //    else {
      //      pidPitchTarget += ((pidPitchTarget > neutralAngle) ? -0.05 : 0.05);  //Pull pid pitch target closer to neutral running angle
      //    }
      pmode = 3;
    }
    else {
      if (goingForwards) {
        PitchTargetAdjuster(pidPitchTarget, neutralAngle, 0.5);
        pmode = 4;
      }
      else {
        PitchTargetAdjuster(pidPitchTarget, -neutralAngle, 0.5);
        pmode = 5;
      }
    }
  }

  //============ END OF PUSHBACK ==============

  pidOutputCurrent = drivePID.Compute(micros());  //Pid constrains the output
  //float current = constrain(filter.getPitch() * -6.0f, -50.0f, 50.0f);
  VESC.setCurrent(pidOutputCurrent);  //Turn motor ON!



  if (motorTestBtn && !digitalRead(uiBtn)) { //Holding UI button keeps drive on for testing (inverted logic level)
    //Do not go into the bit that can disable drive
  }
  else {
    //If speed is high, shut off only if BOTH pads have been off for more than some time
    if (fabs(currentVelKmh) > 1.77f ) {
      if (!mainFootpadPressed && !heelFootpadPressed && (millis() - heelFootpadLiftedStamp) > 700 && (millis() - heelFootpadLiftedStamp) > 700) {
        beeper.AddToQueue(Beeperer::Disarm);
        ControlFunction = ControlOff;
        drivePID.Active(false); //Deactivate drive PID
        pidPitchTarget = 0.0f;  //Reset this just in case
      }
    }
    //If speed is low, shut off when heel pad OR main pad is off, but only if there is low enough roll so that it won't shut off when doing a tight turn
    else {
      if (!heelFootpadPressed && (millis() - heelFootpadLiftedStamp) > 400 && fabs(rollA) < cutoffInhibitRoll) {
        beeper.AddToQueue(Beeperer::Disarm);
        ControlFunction = ControlOff;
        drivePID.Active(false); //Deactivate drive PID
        pidPitchTarget = 0.0f;  //Reset this just in case
      }
      if (!mainFootpadPressed && (millis() - mainFootpadLiftedStamp) > 400 && fabs(rollA) < cutoffInhibitRoll) {
        beeper.AddToQueue(Beeperer::Disarm);
        ControlFunction = ControlOff;
        drivePID.Active(false); //Deactivate drive PID
        pidPitchTarget = 0.0f;  //Reset this just in case
      }
    }
    //If roll is too high for a certain, turn off power since we are fallen over
    if (fabs(rollA) > 60.0f) {
      if (millis() - rollLastOkStamp > 200) {
        beeper.AddToQueue(Beeperer::Disarm);
        ControlFunction = ControlOff;
        drivePID.Active(false); //Deactivate drive PID
        pidPitchTarget = 0.0f;  //Reset this just in case
      }
    }
    else {
      rollLastOkStamp = millis();
    }
  }

}

//Current pitch target, target pitch target, and velocity in deg/s at which current should be moved towards target
void PitchTargetAdjuster(float &current, float target, float velocity) { //
  //static uint32_t pitchStamp = micros();


  //float elapsedSecs = ((double)(micros()-pitchStamp))/1000000.0;
  //pitchStamp = micros();

  //  if(isnan(elapsedSecs) || elapsedSecs > 0.012){
  //    return; //Something wrog with elapsed time calculation, do not do anything this round! Elapsed time updated for next round.
  //  }

  float elapsedSecs = 0.01f;


  //In case we are close enough the target, just set the target there
  if (fabs(current - target) < pushbackAngleTolerance) {
    current = target;
  }
  else {
    if (target > current) {
      current += velocity * elapsedSecs;
    }
    else {
      current -= velocity * elapsedSecs;
    }
  }

}

//Interrupt functions for the IMUs

//void IMU1Int() {
//  intcount++;
//  if (newDataIMU1) missedInts ++;
//  newDataIMU1 = true;
//}
//
//void IMU2Int() {
//  intcount2++;
//  if (newDataIMU2) missedInts ++;
//  newDataIMU2 = true;
//}
//
//void IMU3Int() {
//  intcount3++;
//  if (newDataIMU3) missedInts ++;
//  newDataIMU3 = true;
//}

void UpdateIMU1() {
  static uint64_t imu1stamp = 0;

  imu1ms = (micros() - imu1stamp);
  imu1stamp = micros();

  newDataIMU1 = false;
  IMU_1.readSensor();


  //Flip coordinates (imus are upside down and at a right angle wrt frame)
  //After this, filter goes:
  // Nose up = pitch +
  // Roll left = roll +
  // Yaw left = yaw +
  ay1 = -IMU_1.getAccelX_mss() / 9.81f;
  ax1 = -IMU_1.getAccelY_mss() / 9.81f;
  az1 = -IMU_1.getAccelZ_mss() / 9.81f;
  gy1 = -IMU_1.getGyroX_rads();
  gx1 = -IMU_1.getGyroY_rads();
  gz1 = -IMU_1.getGyroZ_rads();

  //    if (millis() > 15000) {  //Kill output after 15 sec for testing
  //      gx1 = gy1 = gz1 = ax1 = ay1 = az1 = 0.0f;
  //    }
  //
  //    if (millis() > 15000) {  //Change output after 15 sec for testing
  //      ax1 += 0.5;
  //    }

  // update the filter, which computes orientation
  imu1dt = filter.deltatUpdate();
  if (millis() > 10000 && imu1dt > imu1dtmax) imu1dtmax = imu1dt;
  filter.MadgwickUpdate(gx1, gy1, gz1, ax1, ay1, az1, imu1dt);
  //filter.MadgwickUpdate(gx1, gy1, gz1, ax1, ay1, az1, 0.01);
  //filter.updateIMU(gx1, gy1, gz1, ax1, ay1, az1);
  //Serial.println(imu1ms);
}

void UpdateIMU2() {
  static uint64_t imu2stamp = 0;

  imu2ms = (micros() - imu2stamp);
  imu2stamp = micros();

  newDataIMU2 = false;
  IMU_2.readSensor();

  //Flip coordinates (imus are upside down and at a right angle wrt frame)
  //After this, filter goes:
  // Nose up = pitch +
  // Roll left = roll +
  // Yaw left = yaw +
  ay2 = -IMU_2.getAccelX_mss() / 9.81f;
  ax2 = -IMU_2.getAccelY_mss() / 9.81f;
  az2 = -IMU_2.getAccelZ_mss() / 9.81f;
  gy2 = -IMU_2.getGyroX_rads();
  gx2 = -IMU_2.getGyroY_rads();
  gz2 = -IMU_2.getGyroZ_rads();

  //    if (millis() > 15000) {  //Kill output after 15 sec for testing
  //      gx2 = gy2 = gz2 = ax2 = ay2 = az2 = 0.0f;
  //    }

  //    if (millis() > 15000) {  //Kill output after 15 sec for testing
  //      ax2 += 0.5;
  //    }

  // update the filter, which computes orientation
  filter2.MadgwickUpdate(gx2, gy2, gz2, ax2, ay2, az2, filter2.deltatUpdate());
  //filter2.MadgwickUpdate(gx2, gy2, gz2, ax2, ay2, az2, 0.01);
  //filter2.updateIMU(gx2, gy2, gz2, ax2, ay2, az2);
}

void UpdateIMU3() {
  static uint64_t imu3stamp = 0;

  imu3ms = (micros() - imu3stamp);
  imu3stamp = micros();

  newDataIMU3 = false;
  IMU_3.readSensor();

  //Flip coordinates (imus are upside down and at a right angle wrt frame)
  //After this, filter goes:
  // Nose up = pitch +
  // Roll left = roll +
  // Yaw left = yaw +
  ay3 = -IMU_3.getAccelX_mss() / 9.81f;
  ax3 = -IMU_3.getAccelY_mss() / 9.81f;
  az3 = -IMU_3.getAccelZ_mss() / 9.81f;
  gy3 = -IMU_3.getGyroX_rads();
  gx3 = -IMU_3.getGyroY_rads();
  gz3 = -IMU_3.getGyroZ_rads();

  //    if (millis() > 15000) {  //Kill output after 15 sec for testing
  //      gx3 = gy3 = gz3 = ax3 = ay3 = az3 = 0.0f;
  //    }

  //    if (millis() > 15000) {  //Kill output after 15 sec for testing
  //      ax3 += 0.5f;
  //    }

  // update the filter, which computes orientation
  filter3.MadgwickUpdate(gx3, gy3, gz3, ax3, ay3, az3, filter3.deltatUpdate());
  //filter3.MadgwickUpdate(gx3, gy3, gz3, ax3, ay3, az3, 0.01);
  //filter3.updateIMU(gx3, gy3, gz3, ax3, ay3, az3);
}

float Ratecheck(int input) {
  static int tiem = 0;
  static int lastCheck = 0;

  int timeNow = millis();
  float out = (float)(input - lastCheck) / ((float)(timeNow - tiem) / 1000.0);
  lastCheck = input;
  tiem = timeNow;
  return out;
}


bool CalGyroArraysGood(float * xSamples, float * ySamples, float * zSamples, int samplecount, int readcount, byte which) {
  float xBias = 0.0;
  float yBias = 0.0;
  float zBias = 0.0;
  float xMinSample = xSamples[0];
  float xMaxSample = xSamples[0];
  float yMinSample = ySamples[0];
  float yMaxSample = ySamples[0];
  float zMinSample = zSamples[0];
  float zMaxSample = zSamples[0];
  static int printloops = 0;

  for (int sample = 0 ; sample < samplecount ; sample++) {
    xBias += xSamples[sample];
    yBias += ySamples[sample];
    zBias += zSamples[sample];
    xMaxSample = max(xMaxSample, xSamples[sample]);
    xMinSample = min(xMinSample, xSamples[sample]);
    yMaxSample = max(yMaxSample, ySamples[sample]);
    yMinSample = min(yMinSample, ySamples[sample]);
    zMaxSample = max(zMaxSample, zSamples[sample]);
    zMinSample = min(zMinSample, zSamples[sample]);
  }

  xBias = xBias / samplecount;
  yBias = yBias / samplecount;
  zBias = zBias / samplecount;

  if (readcount < samplecount) {
    //Serial.print("printing readcount: "); Serial.println(readcount);
    return false;
  }
  else {
    if (Tog(xBias, xMinSample, clim) || Tog(xBias, xMaxSample, clim) || Tog(yBias, yMinSample, clim) || Tog(yBias, yMaxSample, clim) || Tog(zBias, zMaxSample, clim) || Tog(zBias, zMaxSample, clim)) {
      if (which == 1) {
        IMU_1.setGyroBiasX_rads(xBias * torad);
        IMU_1.setGyroBiasY_rads(yBias * torad);
        IMU_1.setGyroBiasZ_rads(zBias * torad);
      }
      if (which == 2) {
        IMU_2.setGyroBiasX_rads(xBias * torad);
        IMU_2.setGyroBiasY_rads(yBias * torad);
        IMU_2.setGyroBiasZ_rads(zBias * torad);
      }
      if (which == 3) {
        IMU_3.setGyroBiasX_rads(xBias * torad);
        IMU_3.setGyroBiasY_rads(yBias * torad);
        IMU_3.setGyroBiasZ_rads(zBias * torad);
      }
      return true;
    }
    else {
      if (printloops % 500 == 0) Serial.println("Shaking! Keep still!");
      printloops ++;
      return false;
    }
  }
}
bool CalAccelArraysGood(float * xSamples, float * ySamples, float * zSamples, int samplecount, int readcount, byte which) {
  float xAccelBias = 0.0;
  float yAccelBias = 0.0;
  float zAccelBias = 0.0;

  for (int sample = 0 ; sample < samplecount ; sample++) {
    xAccelBias += xSamples[sample];
    yAccelBias += ySamples[sample];
    zAccelBias += zSamples[sample];
  }

  xAccelBias = xAccelBias / samplecount;
  yAccelBias = yAccelBias / samplecount;
  zAccelBias = zAccelBias / samplecount;

  if (readcount < samplecount) {
    return false;
  }
  else {
    if (which == 1) {
      IMU_1.setAccelCalX(xAccelBias, 1.0);
      IMU_1.setAccelCalY(yAccelBias, 1.0);
      IMU_1.setAccelCalZ(zAccelBias + 9.81, 1.0);
      calibStruct.xAccelBiasIMU1 = IMU_1.getAccelBiasX_mss();
      calibStruct.yAccelBiasIMU1 = IMU_1.getAccelBiasY_mss();
      calibStruct.zAccelBiasIMU1 = IMU_1.getAccelBiasZ_mss();
    }
    if (which == 2) {
      IMU_2.setAccelCalX(xAccelBias, 1.0);
      IMU_2.setAccelCalY(yAccelBias, 1.0);
      IMU_2.setAccelCalZ(zAccelBias + 9.81, 1.0);
      calibStruct.xAccelBiasIMU2 = IMU_2.getAccelBiasX_mss();
      calibStruct.yAccelBiasIMU2 = IMU_2.getAccelBiasY_mss();
      calibStruct.zAccelBiasIMU2 = IMU_2.getAccelBiasZ_mss();
    }
    if (which == 3) {
      IMU_3.setAccelCalX(xAccelBias, 1.0);
      IMU_3.setAccelCalY(yAccelBias, 1.0);
      IMU_3.setAccelCalZ(zAccelBias + 9.81, 1.0);
      calibStruct.xAccelBiasIMU3 = IMU_3.getAccelBiasX_mss();
      calibStruct.yAccelBiasIMU3 = IMU_3.getAccelBiasY_mss();
      calibStruct.zAccelBiasIMU3 = IMU_3.getAccelBiasZ_mss();
    }

    return true;
  }
}

void Accelcal() {
  const int samplecount = 200;
  bool IMU1Done = false;
  bool IMU2Done = false;
  bool IMU3Done = false;
  float xSamplesIMU1[samplecount] = { -9999.0};
  float ySamplesIMU1[samplecount] = { -9999.0};
  float zSamplesIMU1[samplecount] = { -9999.0};
  float xSamplesIMU2[samplecount] = { -9999.0};
  float ySamplesIMU2[samplecount] = { -9999.0};
  float zSamplesIMU2[samplecount] = { -9999.0};
  float xSamplesIMU3[samplecount] = { -9999.0};
  float ySamplesIMU3[samplecount] = { -9999.0};
  float zSamplesIMU3[samplecount] = { -9999.0};
  int i_IMU1 = 0;
  int i_IMU2 = 0;
  int i_IMU3 = 0;
  int readcountIMU1 = 0;
  int readcountIMU2 = 0;
  int readcountIMU3 = 0;

  float sinner = 0;
  uint32_t calibBlinkStamp = 0;

  Serial.println("Starting accelerometr calibration, keep still and level");
  IMU_1.setAccelCalX(0.0, 1.0);
  IMU_1.setAccelCalY(0.0, 1.0);
  IMU_1.setAccelCalZ(0.0, 1.0);
  IMU_2.setAccelCalX(0.0, 1.0);
  IMU_2.setAccelCalY(0.0, 1.0);
  IMU_2.setAccelCalZ(0.0, 1.0);
  IMU_3.setAccelCalX(0.0, 1.0);
  IMU_3.setAccelCalY(0.0, 1.0);
  IMU_3.setAccelCalZ(0.0, 1.0);


  while (!IMU1Done || !IMU2Done || !IMU3Done) {

    if (millis() - calibBlinkStamp > 10) {

      if (/*newDataIMU1 && */!IMU1Done) { //Wait until data ready interrupt fires !CalAccelArraysGood(xSamples, ySamples, zSamples, samplecount, readcount, 1)
        newDataIMU1 = false;
        IMU_1.readSensor();
        xSamplesIMU1[i_IMU1] = IMU_1.getAccelX_mss();
        ySamplesIMU1[i_IMU1] = IMU_1.getAccelY_mss();
        zSamplesIMU1[i_IMU1] = IMU_1.getAccelZ_mss();
        i_IMU1++;
        if (i_IMU1 >= samplecount) i_IMU1 = 0;
        readcountIMU1++;
        IMU1Done = CalAccelArraysGood(xSamplesIMU1, ySamplesIMU1, zSamplesIMU1, samplecount, readcountIMU1, 1);
      }
      if (/*newDataIMU2 && */!IMU2Done) { //Wait until data ready interrupt fires !CalAccelArraysGood(xSamples, ySamples, zSamples, samplecount, readcount, 1)
        newDataIMU2 = false;
        IMU_2.readSensor();
        xSamplesIMU2[i_IMU2] = IMU_2.getAccelX_mss();
        ySamplesIMU2[i_IMU2] = IMU_2.getAccelY_mss();
        zSamplesIMU2[i_IMU2] = IMU_2.getAccelZ_mss();
        i_IMU2++;
        if (i_IMU2 >= samplecount) i_IMU2 = 0;
        readcountIMU2++;
        IMU2Done = CalAccelArraysGood(xSamplesIMU2, ySamplesIMU2, zSamplesIMU2, samplecount, readcountIMU2, 2);
      }
      if (/*newDataIMU3 && */!IMU3Done) { //Wait until data ready interrupt fires !CalAccelArraysGood(xSamples, ySamples, zSamples, samplecount, readcount, 1)
        newDataIMU3 = false;
        IMU_3.readSensor();
        xSamplesIMU3[i_IMU3] = IMU_3.getAccelX_mss();
        ySamplesIMU3[i_IMU3] = IMU_3.getAccelY_mss();
        zSamplesIMU3[i_IMU3] = IMU_3.getAccelZ_mss();
        i_IMU3++;
        if (i_IMU3 >= samplecount) i_IMU3 = 0;
        readcountIMU3++;
        IMU3Done = CalAccelArraysGood(xSamplesIMU3, ySamplesIMU3, zSamplesIMU3, samplecount, readcountIMU3, 3);
      }

      calibBlinkStamp = millis();
      sinner += 0.1;
      analogWrite(uiG, (sinf(sinner) + 1) * 110 + 35);
    }

  }

  EEPROM.put(0, calibStruct);  //Update IMU accelerometer calibration

  analogWrite(uiG, 255);
  Serial.println("Accel biases for IMUs computed:");
  Serial.print("IMU1 x: ");
  Serial.print(IMU_1.getAccelBiasX_mss());
  Serial.print(" mss y: ");
  Serial.print(IMU_1.getAccelBiasY_mss());
  Serial.print(" mss z: ");
  Serial.print(IMU_1.getAccelBiasZ_mss());
  Serial.println(" mss");

  Serial.print("IMU2 x: ");
  Serial.print(IMU_2.getAccelBiasX_mss());
  Serial.print(" mss y: ");
  Serial.print(IMU_2.getAccelBiasY_mss());
  Serial.print(" mss z: ");
  Serial.print(IMU_2.getAccelBiasZ_mss());
  Serial.println(" mss");

  Serial.print("IMU3 x: ");
  Serial.print(IMU_3.getAccelBiasX_mss());
  Serial.print(" mss y: ");
  Serial.print(IMU_3.getAccelBiasY_mss());
  Serial.print(" mss z: ");
  Serial.print(IMU_3.getAccelBiasZ_mss());
  Serial.println(" mss");
  delay(500);
  analogWrite(uiG, 0);
  analogWrite(uiR, 0);
  analogWrite(uiB, 0);
}

void Gyrocal(bool extendedCalibration) {
  bool IMU1Done = false;
  bool IMU2Done = false;
  bool IMU3Done = false;
  const int samplecount = 200;
  float xSamplesIMU1[samplecount] = { -9999.0}; //Fill calibration sample arrays with invalid data
  float ySamplesIMU1[samplecount] = { -9999.0};
  float zSamplesIMU1[samplecount] = { -9999.0};
  float xSamplesIMU2[samplecount] = { -9999.0}; //Fill calibration sample arrays with invalid data
  float ySamplesIMU2[samplecount] = { -9999.0};
  float zSamplesIMU2[samplecount] = { -9999.0};
  float xSamplesIMU3[samplecount] = { -9999.0}; //Fill calibration sample arrays with invalid data
  float ySamplesIMU3[samplecount] = { -9999.0};
  float zSamplesIMU3[samplecount] = { -9999.0};
  int i_IMU1 = 0;
  int i_IMU2 = 0;
  int i_IMU3 = 0;
  int readcountIMU1 = 0;
  int readcountIMU2 = 0;
  int readcountIMU3 = 0;
  float sinner = 0.0f;
  uint32_t calibBlinkStamp = 0;


  if (extendedCalibration) {
    Serial.println("Starting gyro + imu bias calibration");
    analogWrite(uiG, 255);  //Show that extended calibration will start by burning the green light for a while
    analogWrite(uiR, 0);
    analogWrite(uiB, 0);
    delay(2000);
    analogWrite(uiG, 0);
  }
  else {
    Serial.println("Starting normal gyro bias calibration");

    analogWrite(uiG, 0);
    analogWrite(uiR, 0);
    analogWrite(uiB, 0);
  }

  while (!IMU1Done || !IMU2Done || !IMU3Done) {

    if (millis() - calibBlinkStamp > 10) {

      if (/*newDataIMU1 && */!IMU1Done) { //Wait until data ready interrupt fires
        newDataIMU1 = false;
        IMU_1.readSensor();
        xSamplesIMU1[i_IMU1] = IMU_1.getGyroX_rads() * todeg;
        ySamplesIMU1[i_IMU1] = IMU_1.getGyroY_rads() * todeg;
        zSamplesIMU1[i_IMU1] = IMU_1.getGyroZ_rads() * todeg;
        i_IMU1++;
        if (i_IMU1 >= samplecount) i_IMU1 = 0;
        readcountIMU1++;
        IMU1Done = CalGyroArraysGood(xSamplesIMU1, ySamplesIMU1, zSamplesIMU1, samplecount, readcountIMU1, 1);
      }

      if (/*newDataIMU2 && */!IMU2Done) { //Wait until data ready interrupt fires
        newDataIMU2 = false;
        IMU_2.readSensor();
        xSamplesIMU2[i_IMU2] = IMU_2.getGyroX_rads() * todeg;
        ySamplesIMU2[i_IMU2] = IMU_2.getGyroY_rads() * todeg;
        zSamplesIMU2[i_IMU2] = IMU_2.getGyroZ_rads() * todeg;
        i_IMU2++;
        if (i_IMU2 >= samplecount) i_IMU2 = 0;
        readcountIMU2++;
        IMU2Done = CalGyroArraysGood(xSamplesIMU2, ySamplesIMU2, zSamplesIMU2, samplecount, readcountIMU2, 2);
      }

      if (/*newDataIMU3 && */!IMU3Done) { //Wait until data ready interrupt fires
        newDataIMU3 = false;
        IMU_3.readSensor();
        xSamplesIMU3[i_IMU3] = IMU_3.getGyroX_rads() * todeg;
        ySamplesIMU3[i_IMU3] = IMU_3.getGyroY_rads() * todeg;
        zSamplesIMU3[i_IMU3] = IMU_3.getGyroZ_rads() * todeg;
        i_IMU3++;
        if (i_IMU3 >= samplecount) i_IMU3 = 0;
        readcountIMU3++;
        IMU3Done = CalGyroArraysGood(xSamplesIMU3, ySamplesIMU3, zSamplesIMU3, samplecount, readcountIMU3, 3);
      }


      calibBlinkStamp = millis();
      sinner += 0.1;
      analogWrite(uiB, (sinf(sinner) + 1) * 110 + 35);
    }
    SegmentWave();
  }
  analogWrite(uiB, 0); //Zero indicators used while calibrating
  analogWrite(uiG, 0);
  analogWrite(uiR, 0);
  analogWrite(segment1, 0); analogWrite(segment2, 0); analogWrite(segment3, 0); analogWrite(segment4, 0); analogWrite(segment5, 0);
  analogWrite(segment6, 0); analogWrite(segment7, 0); analogWrite(segment8, 0); analogWrite(segment9, 0); analogWrite(segment10, 0);

  Serial.println("Done gyro bias calibration: ");
  Serial.print("IMU1 x: "); Serial.print(IMU_1.getGyroBiasX_rads()*todeg); Serial.print(" IMU1 y: "); Serial.print(IMU_1.getGyroBiasY_rads()*todeg);
  Serial.print(" IMU1 z: "); Serial.println(IMU_1.getGyroBiasZ_rads()*todeg);
  Serial.print("IMU2 x: "); Serial.print(IMU_2.getGyroBiasX_rads()*todeg); Serial.print(" IMU2 y: "); Serial.print(IMU_2.getGyroBiasY_rads()*todeg);
  Serial.print(" IMU2 z: "); Serial.println(IMU_2.getGyroBiasZ_rads()*todeg);
  Serial.print("IMU3 x: "); Serial.print(IMU_3.getGyroBiasX_rads()*todeg); Serial.print(" IMU3 y: "); Serial.print(IMU_3.getGyroBiasY_rads()*todeg);
  Serial.print(" IMU3 z: "); Serial.println(IMU_3.getGyroBiasZ_rads()*todeg);



  //delay(500);
}

//Check if two values are within some window of eachother
bool Tog(float one, float two, float window) {
  if (fabs(one - two) < window) return true;
  else return false;
}

void SegmentWave() {
  static uint32_t waveStamp = 0;
  static float sinnerer = 0.0f;
  if (millis() - waveStamp > 10) {
    waveStamp = millis();
    sinnerer -= 0.10;

    analogWrite(segment1, max((int)((sinf(sinnerer)) * 255.0f), 0));
    analogWrite(segment2, max((int)((sinf(sinnerer + 0.2)) * 255.0f), 0));
    analogWrite(segment3, max((int)((sinf(sinnerer + 0.4)) * 255.0f), 0));
    analogWrite(segment4, max((int)((sinf(sinnerer + 0.6)) * 255.0f), 0));
    analogWrite(segment5, max((int)((sinf(sinnerer + 0.8)) * 255.0f), 0));
    analogWrite(segment6, max((int)((sinf(sinnerer + 1.0)) * 255.0f), 0));
    analogWrite(segment7, max((int)((sinf(sinnerer + 1.2)) * 255.0f), 0));
    analogWrite(segment8, max((int)((sinf(sinnerer + 1.4)) * 255.0f), 0));
    analogWrite(segment9, max((int)((sinf(sinnerer + 1.6)) * 255.0f), 0));
    analogWrite(segment10, max((int)((sinf(sinnerer + 1.8)) * 255.0f), 0));

  }
}

void UiFunctions() {
  static uint32_t  buttonPressStamp = 0;
  static bool buttonArm = false; //When true, UI button has been pressed, and action is taken depending on if that was long or short press
  static bool bipOnPressArm = true;  //Quick bip once at the moment UI button is pressed down, enabled again when button raised
  bool infoDumpNow = false;


  if (!digitalRead(uiBtn)) { //Button pressed
    buttonArm = true;
    if (bipOnPressArm) {
      bipOnPressArm = false;
      beeper.AddToQueue(Beeperer::Bip);  //Short press
    }
  }
  else {
    bipOnPressArm = true;
    if (buttonArm) {  //On button release
      buttonArm = false;

      if (millis() - buttonPressStamp > 700) { //Long press, do infodump & show approx cell voltage on lightbar
        beeper.AddToQueue(Beeperer::ImuDiff);
        infoDumpNow = true;
      }
      else {
        //beeper.AddToQueue(Beeperer::Bip);  //Short press, change display mode
        nextScreen = true;
      }
    }
    buttonPressStamp = millis();
  }

  BatteryDisplayer(infoDumpNow);

}

void BatteryDisplayer(bool infoDumpNow) {
  static uint32_t voltOutStamp = 0;
  static uint32_t voltOutState = 0;
  int segms = 0;

  if (!motorTestBtn && infoDumpNow && voltOutState == 0) {
    voltOutState = 1;
    voltOutStamp = millis();
    InfoPrintout();
    //beeper.AddToQueue(Beeperer::Bip);
  }

  if (voltOutState == 0) { //Normal approximate battery display
    if (cellVolts > 4.10f) segms = 10;
    else if (cellVolts > 4.00f) segms = 9;
    else if (cellVolts > 3.95f) segms = 8;
    else if (cellVolts > 3.90f) segms = 7;
    else if (cellVolts > 3.85f) segms = 6;
    else if (cellVolts > 3.80f) segms = 5;
    else if (cellVolts > 3.75f) segms = 4;
    else if (cellVolts > 3.70f) segms = 3;
    else if (cellVolts > 3.60f) segms = 2;
    else if (cellVolts > 3.50f) segms = 1;
    else segms = 0;
    SegmentFiller(segms);
  }
  else if (voltOutState == 1) {   //Show the leading digit of voltage number on bar screen
    SegmentFiller((int)(cellVolts));
    if (millis() - voltOutStamp > 3000) {
      voltOutState = 2;
      voltOutStamp = millis();
      beeper.AddToQueue(Beeperer::Bip);
    }
  }
  else if (voltOutState == 2) {   //Show the second digit of voltage number on bar screen
    SegmentFiller(((int)(cellVolts * 10)) % 10);
    if (millis() - voltOutStamp > 3000) {
      voltOutState = 3;
      voltOutStamp = millis();
      beeper.AddToQueue(Beeperer::Bip);
    }
  }
  else if (voltOutState == 3) {  //Show the devimal digit of voltage number on bar screen
    SegmentFiller(((int)(cellVolts * 100)) % 10);
    if (millis() - voltOutStamp > 3000) {
      voltOutState = 0;
      beeper.AddToQueue(Beeperer::Bip);
    }
  }
}

void SegmentFiller(int segms) {
  segms = constrain(segms, 0, 10);

  analogWrite(segment1, (segms > 0) ? 255 : 0);
  analogWrite(segment2, (segms > 1) ? 255 : 0);
  analogWrite(segment3, (segms > 2) ? 255 : 0);
  analogWrite(segment4, (segms > 3) ? 255 : 0);
  analogWrite(segment5, (segms > 4) ? 255 : 0);
  analogWrite(segment6, (segms > 5) ? 255 : 0);
  analogWrite(segment7, (segms > 6) ? 255 : 0);
  analogWrite(segment8, (segms > 7) ? 255 : 0);
  analogWrite(segment9, (segms > 8) ? 255 : 0);
  analogWrite(segment10, (segms > 9) ? 255 : 0);
}

void AudioHandler() {
  static uint32_t audioStamp = 0;
  if (millis() - audioStamp > 10) {
    audioStamp = millis();
    beeper.Run();
  }
}

void FootpadChecker() {

  static uint32_t footpadStamp = 0;

  if (millis() - footpadStamp > 10) {
    footpadStamp = millis();

    leftNow = analogRead(heelFootpadPin);
    rightNow = analogRead(mainFootpadPin);

    if (heelFootpadPressed && leftNow < heelFootpadLimL) {
      heelFootpadPressed = false;
      heelFootpadLiftedStamp = millis(); //Remember when this was lifted
      //beeper.AddToQueue(Beeperer::BattLow);
    }
    else if (!heelFootpadPressed && leftNow > heelFootpadLimH) {
      heelFootpadPressed = true;
      //beeper.AddToQueue(Beeperer::GPSLock);
    }

    if (mainFootpadPressed && rightNow < mainFootpadLimL) {
      mainFootpadPressed = false;
      mainFootpadLiftedStamp = millis();  //Remember when this was lifted
      //beeper.AddToQueue(Beeperer::HomeLock);
    }
    else if (!mainFootpadPressed && rightNow > mainFootpadLimH) {
      mainFootpadPressed = true;
      //beeper.AddToQueue(Beeperer::StartBeep);
    }
  }
}
void DebugPrintout() {
#ifdef DEBUGPRINTOUT
  static uint32_t dbStamp = 0;
  if (millis() - dbStamp > 100) {
    dbStamp = millis();
    //    Serial.print(filter.getPitch()); Serial.print('\t');
    //    Serial.print(filter2.getPitch()); Serial.print('\t');
    //    Serial.print(filter3.getPitch()); Serial.print('\t');//  Serial.print('\t');

    //    Serial.print(millis()); Serial.print('\t');
    //    Serial.print(lastImuDiscardStamp); Serial.print('\t');
    //    Serial.print(lastDiscarded); Serial.print('\t');// Serial.print('\t');
    //    Serial.print(diff1); Serial.print('\t');
    //    Serial.print(diff2); Serial.print('\t');
    //    Serial.print(diff3); Serial.print('\t');
    //    Serial.print(filter.getPitch()); Serial.print('\t');
    //    Serial.print(filter2.getPitch()); Serial.print('\t');
    //    Serial.print(filter3.getPitch()); Serial.print('\t');
    //    Serial.print(maxDiff); Serial.print('\t');
    //    Serial.print(imu1ms); Serial.print('\t');
    //    Serial.print(imu2ms); Serial.print('\t');
    //    Serial.print(imu3ms); Serial.print('\t');
    //    Serial.print(imu1dt,6); Serial.print('\t');
    //    Serial.println(imu1dtmax,6);


    Serial.println(pitchA);// Serial.print(imu3ms); Serial.print('\t');
    //    Serial.print("lop: "); Serial.println(loopms);



    //    Serial.print(gx2); Serial.print('\t');
    //    Serial.print(gy2); Serial.print('\t');
    //    Serial.println(gz2);  //Serial.print('\t');

    //    Serial.print(ax1); Serial.print('\t');
    //    Serial.print(ay1); Serial.print('\t');
    //    Serial.println(az1);  //Serial.print('\t');

    //    Serial.print(diff1); Serial.print('\t');
    //    Serial.print(diff2); Serial.print('\t');
    //    Serial.println(diff3);  //Serial.print('\t');
  }
#endif
}

void InfoDump(Stream &port) {
  port.println();
  port.println("==================== INFO ====================");

  port.print("Odom: "); port.print(( (float)(calibStruct.totalOdo + unAddedOdo) )*encToMeters, 2); port.print(" m"); port.print('\t');
  port.print("Trip: "); port.print(( (float)(calibStruct.totalOdo + unAddedOdo - tripCompensator) )*encToMeters, 2); port.println(" m");

  port.print("vBatt: "); port.print(VESC.data.inpVoltage, 2); port.print(" V"); port.print('\t');
  port.print("vBattMax: "); port.print(maxBattVoltSeen, 2); port.print(" V"); port.print('\t');
  port.print("vBattMin: "); port.print(minBattVoltSeen, 2); port.println(" V");;

  port.print("tFet: "); port.print(VESC.data.temperatureFet, 2); port.print(" C"); port.print('\t');
  port.print("tFetMax: "); port.print(maxFetTempSeen, 2); port.print(" C"); port.print('\t');
  port.print("tFetMin: "); port.print(minFetTempSeen, 2); port.println(" C");

  port.print("All time max: "); port.print('\t');
  port.print("tFetMax: "); port.print(calibStruct.maxFetTemp, 2); port.print(" C"); port.print('\t');
  port.print("tFetMin: "); port.print(calibStruct.minFetTemp, 2); port.println(" C");

  port.print("MainPadAdc: "); port.print(analogRead(mainFootpadPin)); port.print('\t');
  port.print("HeelPadAdc: "); port.println(analogRead(heelFootpadPin));

  port.print("mAh drawn: "); port.print(VESC.data.ampHours * 1000.0f); port.print('\t');
  port.print("mAh recouped: "); port.println(VESC.data.ampHoursCharged * 1000.0f);

  port.print("Vesc disconnects: "); port.print(vescDisconnects); port.print('\t');
  port.print("Runtime: "); port.print(millis() / 1000 / 60); port.println(" m");
  port.print("Abnormal delays: "); port.print(abnormalDelays); port.print('\t');
  port.print("Max control loop micros: "); port.println(largestControlDelta);

  port.print("Last reset reason:");
  printResetType(port);
  port.println();
  port.println();
}

void InfoPrintout() {
  static uint32_t printStamp = 0;
  if (millis() - printStamp > 100) {
    printStamp = millis();
    InfoDump(Serial);  //USB serial
  }

}

const char* Ep(unsigned int slot) {  //Returns the event string if it is within bounds, else
  if (slot < (sizeof(eventToText) / sizeof(eventToText[0])) ) {
    return eventToText[slot];
  }
  else {
    return "oob!";
  }

}

void EventGeneratorTest() { //Generates new random events for testing, should not be used normally
  static uint32_t eventGenStamp = 0;
  if (millis() > 15000 && millis() - eventGenStamp > 1000) {
    eventGenStamp = millis();
    eventList.PushBack(random(1, 9), millis());
  }
}

void printResetType(Stream &port) {
  if (RCM_SRS1 & RCM_SRS1_SACKERR)   port.println("[RCM_SRS1] - Stop Mode Acknowledge Error Reset");
  if (RCM_SRS1 & RCM_SRS1_MDM_AP)    port.println("[RCM_SRS1] - MDM-AP Reset");
  if (RCM_SRS1 & RCM_SRS1_SW)        port.println("[RCM_SRS1] - Software Reset");
  if (RCM_SRS1 & RCM_SRS1_LOCKUP)    port.println("[RCM_SRS1] - Core Lockup Event Reset");
  if (RCM_SRS0 & RCM_SRS0_POR)       port.println("[RCM_SRS0] - Power-on Reset");
  if (RCM_SRS0 & RCM_SRS0_PIN)       port.println("[RCM_SRS0] - External Pin Reset");
  if (RCM_SRS0 & RCM_SRS0_WDOG)      port.println("[RCM_SRS0] - Watchdog(COP) Reset");
  if (RCM_SRS0 & RCM_SRS0_LOC)       port.println("[RCM_SRS0] - Loss of External Clock Reset");
  if (RCM_SRS0 & RCM_SRS0_LOL)       port.println("[RCM_SRS0] - Loss of Lock in PLL Reset");
  if (RCM_SRS0 & RCM_SRS0_LVD)       port.println("[RCM_SRS0] - Low-voltage Detect Reset");
}

float fmap(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

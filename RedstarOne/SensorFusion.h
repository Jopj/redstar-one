//=============================================================================================
// SensorFusion.h
//=============================================================================================
//
// Madgwick's implementation of Mahony's AHRS algorithm.
// See: http://www.x-io.co.uk/open-source-imu-and-ahrs-algorithms/
//
// Date			Author			Notes
// 29/09/2011	SOH Madgwick    Initial release
// 02/10/2011	SOH Madgwick	Optimised for reduced CPU load
// 19/02/2012	SOH Madgwick	Magnetometer measurement is normalised
// 23/11/2017   Aster			Optimised time handling and melted in one library
//
//=============================================================================================
#ifndef SensorFusion_h
#define SensorFusion_h

#include <math.h>
#include "Arduino.h"

//--------------------------------------------------------------------------------------------
// Variable declaration

struct filter_quaternion{
  float x;
  float y;
  float z;
  float w;
};


class SF {
//-------------------------------------------------------------------------------------------
// Function declarations

public:
   
	SF();
	SF(float beeta);

	void MahonyUpdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz, float deltat);
	void MahonyUpdate(float gx, float gy, float gz, float ax, float ay, float az, float deltat);

	void MadgwickUpdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz, float deltat);
  void MadgwickUpdate(float gx, float gy, float gz, float ax, float ay, float az, float deltat);

	//these values are already defined by arduino
	//const float DEG_TO_RAD = 0.0174532925199433f; //PI/180.0f;
	//const float RAD_TO_DEG = 57.29577951308233f; //180.0f/PI


  float deltatUpdate (){  //This should hopefully not have the overflow issue
    Now = (uint32_t)micros();
    deltat = (Now - lastUpdate); // set integration time by time elapsed since last filter update
    lastUpdate = Now;
    float measDt = (float)(((double)deltat)/1000000.0);
    if(measDt > 0.05f) measDt = 0.05f;  //Limit this in case there's a hiccup. Min rate 20 hz.
    return measDt;
  }
	float getRoll() {
		if (!anglesComputed) computeAngles();
		return roll * RAD_TO_DEG;
	}
	float getPitch() {
		if (!anglesComputed) computeAngles();
		return pitch * RAD_TO_DEG;
	}
	float getYaw() {
		if (!anglesComputed) computeAngles();
		return yaw * RAD_TO_DEG + 180.0f;
	}
	float getRollRadians() {
		if (!anglesComputed) computeAngles();
		return roll;
	}
	float getPitchRadians() {
		if (!anglesComputed) computeAngles();
		return pitch;
	}
	float getYawRadians() {
		if (!anglesComputed) computeAngles();
		return yaw;
	}
  filter_quaternion getQuaternion() {
    if (!anglesComputed) computeAngles();
    filter_quaternion q;
    q.x = q1;
    q.y = q2;
    q.z = q3;
    q.w = q0;
    return q;
  }
  void setOrientation(filter_quaternion q){
    q1 = q.x;
    q2 = q.y;
    q3 = q.z;
    q0 = q.w;
  }
  
	float q0, q1, q2, q3;	// quaternion of sensor frame relative to auxiliary frame

private:
	float beta;				//Madgwick: 2 * proportional gain
	float twoKp;			//Mahony: 2 * proportional gain (Kp)
	float twoKi;			//Mahony: 2 * integral gain (Ki)

	float integralFBx, integralFBy, integralFBz;  // integral error terms scaled by Ki
	bool anglesComputed;
	static float invSqrt(float x);
	void computeAngles();
	float roll, pitch, yaw;
  uint32_t Now,lastUpdate;
	float deltat;

};

#endif

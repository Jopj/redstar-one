#ifndef RedstarClasses_cpp
#define RedstarClasses_cpp

#include <Arduino.h>


class Beeperer {
  public:

    enum Tunes
    {
      Tester,
      StartBeep,
      Arm,
      Disarm,
      BattLow,
      BattVeryLow,
      Overspeed,
      OvercurrentMot,
      OvercurrentBat,
      RegencurrentWarn,
      WarmtempFet,
      OvertempFet,
      Pushback,
      Critical,
      ReadyMode,
      Bip,
      BluetoothConnect,
      BluetoothDisconnect,
      ImuDiff
    };

    Beeperer(uint8_t pin);
    void Run();                       //Beeps
    void AddToQueue(unsigned int beepType);    //Tries to add a tune in the queue, does not do so if it is full
    bool Beepables();                 //True if there is something in the queue

  private:
    int32_t _GetNext(); //Returns index to next tune to play from queue, or negative if there is none
    int32_t _queue[5];  //A queue of integers representing tunes in the tune list. Negative numbers means empty
    uint8_t _pin;
    bool _beepables;     //If there is anything to do
    uint32_t _noteNumber;   //Position in the note field
    uint32_t _noteCounter;   //Time position within a note, in beats (One beat is the time between calls to the Run() method

    uint32_t _queueDelay;  //How many beats of delay between cycles
    int32_t _currentIndex;  //Index of currently playing tune

    struct note {
      uint32_t freq;   //Frequency of note
      uint32_t tim;    //How long the note is, in beats
    };
    struct tune {
      note* notelist;    //Pointer to an array of notes
      uint32_t noteCount;  //The amount of notes in the array
    };

    note tester[11] = { {3200, 2} , {0, 1} , {3000, 2}, {0, 1} , {2800, 2}, {0, 1}, {3200, 2} , {0, 1} , {3000, 2}, {0, 1} , {2800, 4}};
    note startbeep[5] = { {2400, 1} , {0, 1} , {2000, 1}, {0, 1} , {2800, 4}};
    note arm[7] = { {3200, 1} , {0, 1} , {3200, 1}, {0, 1} , {3200, 1}, {0, 1} , {3700, 1}};
    note disarm[7] = { {3700, 1} , {0, 1} , {3700, 1}, {0, 1} , {3700, 1}, {0, 1} , {3200, 2}};
    note battlow[5] = { {2600, 2} , {0, 2} , {2800, 2}, {0, 2} , {2600, 4}};
    note battverylow[5] = { {2600, 2} , {0, 2} , {2600, 2}, {0, 2} , {2600, 4}};
    note overspeed[6] = { {3200, 2} , {0, 2} , {3200, 2}, {0, 2} , {3200, 2}, {0, 8}};
    note overcurmot[4] = { {3200, 2} , {0, 2} , {2800, 4}, {0, 8}};
    note overcurbat[6] = { {3200, 2} , {0, 2} , {2800, 2}, {0, 2} , {2800, 2}, {0, 8}};
    note regcurbat[4] = { {2800, 2} , {0, 2} , {2800, 2} , {0, 8}};
    note warmtempfet[8] = { {2800, 2} , {0, 2} , {3200, 4}, {0, 2} , {3600, 2}, {0, 2}, {4000, 2}, {0, 8} };
    note overtempfet[6] = { {2800, 2} , {0, 2} , {3200, 4}, {0, 2} , {2800, 2}, {0, 8} };
    note pushback[7] = { {2800, 2} , {0, 2} , {3000, 4}, {0, 2} , {3200, 2}, {0, 2} , {3400, 2}};
    note critical[1] = { {3200, 6}};
    note readymode[5] = { {2400, 1} , {0, 1} , {2400, 1}, {0, 1} , {3400, 6}};
    note bip[1] = { {3800, 1}};
    note btconnect[12] = { {2400, 1} , {2600, 1} , {2800, 1} , {3000, 1} , {3200, 1} , {3400, 1} , {2600, 1} , {2800, 1} , {3000, 1} , {3200, 1} , {3400, 1} , {3600, 1}};
    note btdisconnect[12] = { {3600, 1} , {3400, 1} , {3200, 1} , {3000, 1} , {2800, 1} , {2600, 1} , {3400, 1} , {3200, 1} , {3000, 1} , {2800, 1} , {2600, 1} , {2400, 1}};
    note imudiff[2] = { {1500, 10} , {0, 8}};

    tune tunelist[19] = {
      {tester, 11},
      {startbeep, 5},
      {arm, 7},
      {disarm, 7},
      {battlow, 5},
      {battverylow, 5},
      {overspeed, 6},
      {overcurmot, 4},
      {overcurbat, 6},
      {regcurbat, 4},
      {warmtempfet, 8},
      {overtempfet, 6},
      {pushback, 7},
      {critical, 1},
      {readymode, 5},
      {bip, 1},
      {btconnect, 12},
      {btdisconnect, 12},
      {imudiff, 2}
    };

};

class EventList {
  
  struct event{
    int event;
    uint32_t timestamp;
  };
  
  public:
    EventList(int numEvents);   //Initialize event list with n amount of slots in it. Hardcoded to 8 now though
    int PushBack(int id, uint32_t timestamp);   //Push an event in the first free slot, or if no free slots remain,
    event GetEventAtSlot(int slot);  //Returns event at specified slot, event negative if slot out of bounds

  private:
    bool InRange(int slot);    //True if asked slot was in range, false if not
    int GetSlotCount();  //returns how many slots there is
    event eventList[8] = {{0,0}};  //Actual event list
};























#endif


#include <Arduino.h>
#include "TemeriPID.h"


TemeriPID::TemeriPID(const float *input, float *setpoint, float *p, float *i, float *d, float *lolim, float *hilim) {
  _setpoint = setpoint;
  _input = input;

  kp = p;
  ki = i;
  kd = d;

  _lolim = lolim;
  _hilim = hilim;

  _ITerm = _error = _dInput = _output = _lastInput = 0.0;
  _lastTime = micros();

  _active = true;
}

float TemeriPID::Compute(uint32_t now) {
  if (!_active) {
    return 0.0; //If not active, zero output
  }

  /*Compute all the working error variables*/
  //float timeFactor = ((float)(now - _lastTime)) / 100.0; //Compensate for call time differences
  float _error = *_setpoint - *_input;
  _ITerm += *ki * _error; //* timeFactor;

  if (_ITerm > *_hilim) _ITerm = *_hilim;                     //If integrator exeeds limits, clamp it.
  else if (_ITerm < *_lolim) _ITerm = *_lolim;               //Same for the other direction.

  float _dInput = (*_input - _lastInput);// timeFactor;        //Derivative of input instead of error to get rid of spiking

  /*Compute PID Output*/
  _output = (*kp * _error + _ITerm - *kd * _dInput);            //Flip d-term to get right direction
  if (_output > *_hilim) _output = *_hilim;                   //Same clamping as before, now for whole _output.
  else if (_output < *_lolim) _output = *_lolim;                //If implementing onthefly changing remember to do this at change too.

  /*Remember some variables for next time*/
  _lastInput = *_input;
  _lastTime = now;
  return _output;


}
void TemeriPID::ResetI() {
  _ITerm = 0.0;
}

void TemeriPID::Active(bool active) {
  if (active) {
    _active = true;
  }
  else {
    _active = false;
    Initialize();   
  }
}
void TemeriPID::Initialize() { //Resets PID error terms, inhibits jumping when restarting PID. Do not use directly, "PidActive" function takes care of this.
  _lastInput = *_input;
  _ITerm = 0.0;
  if (_ITerm > *_hilim) _ITerm = *_hilim;
  else if (_ITerm < *_lolim) _ITerm = *_lolim;
}

void TemeriPID::SetOutputLimits(float lolim, float hilim) {
  *_lolim = lolim;
  *_hilim = hilim;
}
